<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $fillable = [
      'name',
      'data'
    ];

    public function pages() {
      return $this->hasMany('App\Page');
    }

}
