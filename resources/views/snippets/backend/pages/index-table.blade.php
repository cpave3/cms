<table class="table table-bordered" id="data-table">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="20">ID</td>
      <td>Title</td>
      <td width="120">Page Level</td>
      <td width="150">Parent</td>
      <td width="100">Date</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($pages as $page)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.pages.destroy', $page->id]]) !!}
        <a href="{{route('admin.pages.edit', $page->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$page->id}}</td>
        <td>{{$page->title}}</td>
        <td>{{$page->depth}}</td>
        <td>{{$page->parent()->get() ? $page->parent()->first()['title'] : "Root Level"}}</td>
        <td>
          <abbr title="{{$page->dateFormatted(true)}}">{{$page->dateFormatted()}}</abbr>
          {!! $page->publicationLabel !!}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
