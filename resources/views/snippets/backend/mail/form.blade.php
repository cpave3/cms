<div class="col-sm-8 col-md-9">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body ">
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
          {!! Form::label('name', 'Message Name (for your own reference)')!!}
          {!! Form::text('name', null, ['class' => 'form-control', 'required'=>'required'])!!}
          @if ($errors->has('name'))
            <span class="help-block">{{$errors->first('name')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('from_email') ? 'has-error' : ''}}">
          {!! Form::label('from_email', 'From Email for this Message')!!}
          {!! Form::text('from_email', null, ['class' => 'form-control', 'required'=>'required'])!!}
          @if ($errors->has('from_email'))
            <span class="help-block">{{$errors->first('from_email')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('from_name') ? 'has-error' : ''}}">
          {!! Form::label('from_name', 'From Name for this Message')!!}
          {!! Form::text('from_name', null, ['class' => 'form-control', 'required'=>'required'])!!}
          @if ($errors->has('from_name'))
            <span class="help-block">{{$errors->first('from_name')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('subject') ? 'has-error' : ''}}">
          {!! Form::label('subject', 'Subject Line')!!}
          {!! Form::text('subject', null, ['class' => 'form-control', 'required'=>'required'])!!}
          @if ($errors->has('subject'))
            <span class="help-block">{{$errors->first('subject')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('lists') ? 'has-error' : ''}}">
          {!! Form::label('Recipient Groups')!!}
          {{-- {!! Form::select('lists[]', App\Subscription::pluck('name', 'id') ,null, ['class' => 'form-control', 'placeholder' => 'Select one or more lists', 'multiple'])!!} --}}
          <select multiple="multiple" name="lists[]" id="lists" class="form-control" required="required">
          @foreach($lists as $list)
              {{-- @foreach($mail->subscriptions as $mList) --}}
                  <option value="{{$list->id}}" @if (isset($mail) && $mail->subscriptions->contains($list->id)) selected="selected" @endif>{{$list->name}}</option>
              {{-- @endforeach --}}
          @endforeach
          </select>

          {{-- {!! Form::select('lists', null, ['class' => 'form-control', 'multiple'])!!} --}}
          @if ($errors->has('lists'))
            <span class="help-block">{{$errors->first('lists')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('body') ? 'has-error' : ''}}">
          {!! Form::label('body')!!}
          {!! Form::textarea('body', null, ['class' => 'form-control tinymce'])!!}
          @if ($errors->has('body'))
            <span class="help-block">{{$errors->first('body')}}</span>
          @endif
        </div>



    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      {!! Form::submit(($mail->exists ? 'Update' : 'Save'), ['class' => 'btn btn-primary']) !!}
      <a href="{{route('admin.lists.index')}}" class="btn btn-default">Cancel</a>
    </div>
  </div>
  <!-- /.box -->
</div>

<div class="col-sm-4 col-md-3">
  <div class="box">
    <div class="box-body">
      <h3>Available Mail Variables</h3>
      <code>[name]</code> - The subscribers full name
    </div>
  </div>
</div>
