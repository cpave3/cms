<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
          'title' => 'required',
          'slug' => 'required|unique:pages',
          'body' => 'required',
          'excerpt' => 'required'
          // 'published_at' => 'date_format:Y-m-d H:i:s',
          // 'category_id' => 'required',

        ];

        switch($this->method()) {
          case 'PUT';
          case 'PATCH';
            $rules['slug'] = 'required|unique:pages,slug,' . $this->route('page');
            break;
        }

        return $rules;
    }
}
