@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.blog.index')}}"><i class="fa fa-pencil"></i> Mail</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Edit Mail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {!! Form::model($mail, [
            'method' => 'PUT',
            'route' => ['admin.mail.update', $mail->id],
            'files' => TRUE,
            'id' => 'post-form'
            ]) !!}
          @include('snippets.backend.mail.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.blog')
@endsection
