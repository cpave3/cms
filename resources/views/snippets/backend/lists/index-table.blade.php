<table class="table table-bordered">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="200">Title</td>
      <td width="120">Members</td>
      <td>Description</td>
      <td width="200">Default From Email</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($subscriptions as $subscription)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.lists.destroy', $subscription->id]]) !!}
        <a href="{{route('admin.lists.edit', $subscription->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$subscription->name}}</td>
        <td>{{$subscription->subscribers->count()}}</td>
        <td>{{$subscription->description}}</td>
        <td>{{$subscription->from_email}}</td>
      </tr>
    @endforeach
  </tbody>
</table>
