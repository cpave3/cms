<?php

namespace App\Http\Requests;

use App\Subscriber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SubscribeRequest extends FormRequest
{
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			  'name' => 'required',
			  'email' => 'required|email|unique:subscribers',
		];
	}

	public function messages()
	{
		return [
			  'name.required' => 'Please enter your name',
			  'email.required' => 'Please enter your email address',
			  'email.unique' => 'Someone has subscribed to QAI with that email'
		];
	}

}
