<?php

namespace App\Http\Controllers\Backend;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\database\QueryException;
use App\User;
use App\Http\Requests\UserRequest;

class AdminUserController extends BackendController
{


	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$role = User::checkUserRole();
		//$users = User::getAllUsers();
		//return view('admin.users.index', compact('users'));
		return view('admin.users.index', compact('role'));
		//return view('admin.users.index');
	}

	public function edit(User $user)
	{
		$roles = Role::all();
		$user = User::find($user->id);
		return view('admin.users.edit', compact('user', 'roles') );
	}

	public function create()
	{
		$roles = Role::all();
		return view('admin.users.create', compact('roles'));
	}

	public function store(UserRequest $request)
	{
		$data = $request->all();
		$data['password'] = bcrypt($data['password']);
		User::create($data);
		return redirect(route('admin.users.index'))->with('success', 'A new User was successfully created');
	}

	public function destroy($user)
	{
		try {
			User::destroy($user);
		} catch (QueryException $e) {
			$cannotDeleteUser = true;
			$user = User::findOrFail($user);
			$userName = $user->name;
			return view('admin.users.index', compact('cannotDeleteUser', 'userName'));
		}
		return redirect(route('admin.users.index'))->with('success', 'A new User was successfully deleted');
	}

	public function update(UserRequest $request, User $user)
	{
		User::updateUser($request, $user);
		return redirect(route('admin.users.index'))->with('success', 'User was successfully updated');
	}

	public function changePassword($user)
	{
		$user = User::find($user);
		//$user = User::where('slug', $user)->first();
		return view('admin.users.change-password', compact('user'));
	}

	public function updatePassword(UserRequest $request, User $user)
	{
		// die('test');
		// return "test";
		User::changePassword($request, $user);
		return redirect(route('admin.users.index'))->with('success', 'Password was successfully updated');
	}

	public function show($id) {
		return redirect('/');;
	}

}
