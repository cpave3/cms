<table class="table table-bordered" id="data-table">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="20">ID</td>
      <td>Title</td>
      <td width="120">Author</td>
      <td width="150">Category</td>
      <td width="150">Date</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($posts as $post)
      <tr>
        <td>
        {!! Form::open(['method' => 'PUT', 'action' => ['Backend\AdminBlogController@restore', $post->id], 'class' => 'form-inline']) !!}
          <button type="submit" class="btn btn-xs btn-success">
            <i class="fa fa-undo"></i>
          </button>
        {!! Form::close() !!}
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.blog.force-destroy', $post->id], 'class' => 'form-inline']) !!}
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-times"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$post->id}}</td>
        <td>{{$post->title}}</td>
        <td>{{$post->author->name}}</td>
        <td>{{$post->category->title}}</td>
        <td>
          <abbr title="{{$post->dateFormatted(true)}}">{{$post->dateFormatted()}}</abbr>
          {!! $post->publicationLabel !!}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
