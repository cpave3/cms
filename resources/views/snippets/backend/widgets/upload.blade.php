@if (isset($page->id) || isset($post->id))
  <div class="box">
    <style scoped>
    /* TODO: Refactor and Sassify */
    @keyframes spin {
      from { transform: rotate(0deg); }
      to { transform: rotate(360deg); }
    }
    .off {
      display: none;
    }
    .spinner {
      display: inline-block;
      animation: spin 1s infinite linear;
    }
      form {
        display: inline-block;
      }

      .btn-delete {
        background-color: transparent;
        border: none;
        color: red;
      }

      .btn-info {
        background-color: transparent;
        border: none;
        color: blue;
      }

      .btn-info:hover {
        background-color: transparent;
        border: none;
        color: blue;
      }
    </style>
    <div class="box-header with-border">
      <h3 class="box-title">Attachments</h3>
    </div>
    <div class="box-body">

    <div id="attachments">
      @if (isset($page) && $page->attachments->count() > 0)
        <ul>
        <style scoped>
          form {
            display: inline-block;
          }

          .btn-delete {
            background-color: transparent;
            border: none;
            color: red;
          }
        </style>
          @foreach ($page->attachments as $attachment)
            <li><a href="{{$attachment->path}}" target="_blank">{{$attachment->name}}</a> - {{$attachment->size}} |
              {!! Form::open(['method' => 'DELETE', 'class' => 'ajax-delete-form', 'route' => ['admin.uploads.destroy', $attachment->id]]) !!}
              <button type="submit" class="btn-delete">
                Delete
              </button>
              {!! Form::close() !!}
              |
                  {!! Form::open(['method' => 'PUT', 'class' => 'ajax-hide-form', 'route' => ['admin.uploads.hideToggle', $attachment->id]]) !!}
                  <button type="submit" class="btn-info">
                    @if ($attachment->hidden == 1)
                      Unhide
                    @elseif ($attachment->hidden == 0)
                      Hide
                    @endif
                  </button>
                  {!! Form::close() !!}
          @endforeach
        </ul>
      @elseif (isset($post) && $post->attachments->count() > 0)
        <ul>
        <style scoped>
          form {
            display: inline-block;
          }

          .btn-delete {
            background-color: transparent;
            border: none;
            color: red;
          }
        </style>
          @foreach ($post->attachments as $attachment)
            <li><a href="{{$attachment->path}}" target="_blank">{{$attachment->name}}</a> - {{$attachment->size}} |
              {!! Form::open(['method' => 'DELETE', 'class' => 'ajax-delete-form', 'route' => ['admin.uploads.destroy', $attachment->id]]) !!}
              <button type="submit" class="btn-delete">
                Delete
              </button>
              {!! Form::close() !!}
              |
                  {!! Form::open(['method' => 'PUT', 'class' => 'ajax-hide-form', 'route' => ['admin.uploads.hideToggle', $attachment->id]]) !!}
                  <button type="submit" class="btn-info">
                    @if ($attachment->hidden == 1)
                      Unhide
                    @elseif ($attachment->hidden == 0)
                      Hide
                    @endif
                  </button>
                  {!! Form::close() !!}
          @endforeach
        </ul>
      @else
        <span>No Attachments to show</span>
      @endif
    </div>
          <hr>
          {!! Form::open([
            'method' => 'POST',
            'route' => 'admin.uploads.store',
            'files' => TRUE,
            'id' => 'attachment-form'
            ]) !!}
            {!! Form::hidden('type', 1) !!}

            @if (isset($page))
              {!! Form::hidden('page', $page->id) !!}
            @elseif (isset($post))
              {!! Form::hidden('post', $post->id) !!}
            @endif
          <div class="form-group">
            {!! Form::label('name')!!}
            {!! Form::text('name', null, ['class' => 'form-control', 'required'])!!}
          </div>
          <div class="form-group">
            {!! Form::label('file')!!}
            {!! Form::file('file', null, ['class' => 'form-control', 'required'])!!}
          </div>
          <div class="form-group">
            {!! Form::label('hidden')!!}
            {!! Form::checkbox('hidden', null,  ['class' => 'form-control', 'value'=>'1'])!!}
          </div>
          {{-- <input type="submit" value="Add New Attachment" class="btn btn-success"/> --}}
          <button type="submit" name="submit" class="btn btn-success" id="attachment-submit">
            <span class="upload-button-text">Upload Attachment</span>
            <i class="fa fa-circle-o-notch off"></i>
          </button>

          {!! Form::close() !!}
        </div>
      </div>
@endif
