@if (isset($pages))
  <ul class="pages-list">
    @foreach ($pages as $page)
      <li data-id="{{$page->id}}"><a href="#">{{$page->title}}</a></li>
    @endforeach
  </ul>
@endif
