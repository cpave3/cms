@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.menus.index')}}"><i class="fa fa-envelope"></i> Mail</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Create New Mail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {!! Form::model($mail, [
            'method' => 'POST',
            'route' => 'admin.mail.store',
            'files' => TRUE,
            'id' => 'mail-form'
            ]) !!}
          @include('snippets.backend.mail.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  {{-- @include('snippets.backend.scripts.blog') --}}
@endsection
