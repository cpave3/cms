<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('hero_text')->nullable();
            $table->text('red1_text')->nullable();
            $table->string('red1_link')->nullable();
            $table->text('square1_text')->nullable();
            $table->string('square1_link')->nullable();
            $table->text('square2_text')->nullable();
            $table->string('square2_link')->nullable();
            $table->text('square3_text')->nullable();
            $table->string('square3_link')->nullable();
            $table->integer('top_menu_id')->nullable();
            $table->integer('footer_menu_id')->nullable();
            $table->text('footer_contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
