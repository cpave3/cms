<table class="table table-bordered" id="data-table">
    <thead>
    <tr>
        <td width="80">Action</td>
        <td>Title</td>
        <td>Event Date</td>
        <td>Location</td>
        <td>Excerpt</td>
        {{-- <td>Body</td> --}}
    </tr>
    </thead>
    <tbody>
    @foreach ($events as $event)
        <tr>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['admin.events.destroy', $event->id]]) !!}
                <a href="{{route('admin.events.edit', $event->id)}}" class="btn btn-xs btn-default">
                    <i class="fa fa-edit"></i>
                </a>
                <button type="submit" class="btn btn-xs btn-danger">
                    <i class="fa fa-trash"></i>
                </button>
                {!! Form::close() !!}
            </td>
            <td>{{ $event->title }}</td>
            <td>{{ Carbon\Carbon::parse($event->event_date)->format('d/m/Y')}}</td>
            <td>{{ $event->location }}</td>
            <td>{{ $event->excerpt }}</td>
            {{-- <td>{{ $event->body }}</td> --}}

        </tr>
    @endforeach
    </tbody>
</table>
