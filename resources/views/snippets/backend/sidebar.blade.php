<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    @php
      $currentUser = Auth::user();
    @endphp
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{$currentUser->gravatar()}}" class="img-circle" alt="{{$currentUser->name}}">
      </div>
      <div class="pull-left info">
        <p>{{$currentUser->name}}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li>
        <a href="{{route('admin.dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      @if(auth()->user()->role->name == 'Super Admin' || auth()->user()->role->name == 'Admin')
        <li>
          <a href="{{route('admin.users.index')}}">
            <i class="fa fa-user"></i> <span>Users</span>
          </a>
        </li>
      @endif
      <li class="treeview">
        <a href="#">
          <i class="fa fa-pencil"></i>
          <span>Blog</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.blog.index')}}"><i class="fa fa-circle-o"></i> All Posts</a></li>
          <li><a href="{{route('admin.blog.create')}}"><i class="fa fa-circle-o"></i> Add New</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-file"></i>
          <span>Pages</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.pages.index')}}"><i class="fa fa-circle-o"></i> All Pages</a></li>
          <li><a href="{{route('admin.pages.create')}}"><i class="fa fa-circle-o"></i> Add New</a></li>
          <li><a href="{{route('admin.home.edit')}}"><i class="fa fa-circle-o"></i> Edit Home Page</a></li>
        </ul>
      </li>
      <li><a href="{{route('admin.categories.index')}}"><i class="fa fa-folder"></i> <span>Categories</span></a></li>
      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-envelope"></i>
          <span>Mailing </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.mail.index')}}"><i class="fa fa-circle-o"></i> All Mail</a></li>
          <li><a href="{{route('admin.mail.create')}}"><i class="fa fa-circle-o"></i> Add New</a></li>
          <li><a href="{{route('admin.lists.index')}}"><i class="fa fa-circle-o"></i> Mailing Lists</a></li>
        </ul>
      </li> --}}
      <li class="treeview">
        <a href="#">
          <i class="fa fa-calendar"></i>
          <span>Events </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.events.index')}}"><i class="fa fa-circle-o"></i> All Events</a></li>
          <li><a href="{{route('admin.events.create')}}"><i class="fa fa-circle-o"></i> Add New</a></li>
        </ul>
      </li>

      {{-- <li class="treeview">
        <a href="#">
          <i class="fa fa-upload"></i>
          <span>Uploads</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.uploads.index')}}"><i class="fa fa-circle-o"></i> All Media</a></li>
          <li><a href="#upload-modal" data-toggle="modal"><i class="fa fa-circle-o"></i> Add Media</a></li>
          <li><a href="{{route('admin.uploads.index')}}"><i class="fa fa-circle-o"></i> All Attachments</a></li>
          <li><a href="{{route('admin.uploads.create')}}"><i class="fa fa-circle-o"></i> Add Attachment</a></li>
        </ul>
      </li> --}}

      <li class="treeview">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Menu Builder</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{route('admin.menus.index')}}"><i class="fa fa-circle-o"></i> All Menus</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="{{url('/')}}">
          <i class="fa fa-globe"></i>
          <span>View Site</span>
          <span class="pull-right-container">
          </span>
        </a>
        </ul>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
