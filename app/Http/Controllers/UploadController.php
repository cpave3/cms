<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upload;
use App\Page;
use App\Post;
class UploadController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $uploads = Upload::all();
        return view('admin.uploads.index', compact('uploads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    private function handleRequest($request) {
      $data = $request->all();
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $data['originalFile'] = $image->getClientOriginalName();
        $data['size'] = $image->getClientSize();
        $data['fileName'] = time() . "-" . $image->getClientOriginalName();
        $fileName = $data['fileName'];

        $destination = 'uploads';
        $successUploaded = $image->move($destination, $fileName);

        $data['image'] = $fileName;
      }

      // HACK: TBH cannot be bothered making this do anything apart from work rn
      // TODO: Refactor into more elegant code  whithout the need for duplication

      if ($request->hasFile('file')) {
        $file = $request->file('file');
        $data['originalFile'] = $file->getClientOriginalName();
        $data['size'] = $file->getClientSize();
        $data['fileName'] = time() . "-" . $file->getClientOriginalName();
        $fileName = $data['fileName'];

        $destination = 'uploads';
        $successUploaded = $file->move($destination, $fileName);

        $data['file'] = $fileName;
      }
      return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->handleRequest($request);
        // dd($data);

        if (isset($data['hidden'])) {
          $data['hidden'] = 1;
        } else {
          $data['hidden'] = 0;
        }

        if (!isset($data['type'])) {
          //If type not set, set unassigned
          $data['type'] = 0; //unassigned
        }

        if (isset($data['page'])) {
          $page = Page::findOrFail($data['page']);
          $data['user_id'] = $request->user()->id;
          $upload = $page->attachments()->create($data);
        } elseif (isset($data['post'])) {
          $post = Post::findOrFail($data['post']);
          $data['user_id'] = $request->user()->id;
          $upload = $post->attachments()->create($data);
        } else {
          $upload = $request->user()->uploads()->create($data);
        }

        return "Success";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $upload = Upload::findOrFail($id);
        if (file_exists('uploads/'.$upload->fileName)) {
          unlink('uploads/'.$upload->fileName);
        }
        $upload->delete();

    }

    public function ajaxPages($id) {
      $page = Page::findOrFail($id);

      return view('admin.pages.ajax.uploads', compact('page'));

    }

    public function ajaxPosts($id) {
      $page = Post::findOrFail($id);

      return view('admin.pages.ajax.uploads', compact('page'));

    }

    public function hideToggle($id) {
      $upload = Upload::findOrFail($id);

      if ($upload->hidden == 0) {
        $upload->hidden = 1;
      } else {
        $upload->hidden = 0;
      }

      $upload->save();
    }
}
