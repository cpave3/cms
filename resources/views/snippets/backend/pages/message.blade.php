{{-- Special messsages --}}
@if (session('trash'))
  <div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    @php
      list($message, $pageId) = session('trash');
    @endphp
    <h4><i class="fa fa-warning"></i> &nbsp;Page Trashed</h4>
    {!! Form::open(['method' => 'PUT', 'action' => ['Backend\AdminPageController@restore', $pageId], 'class' => 'form-inline']) !!}
      {{$message}}
      <div class="form-group">
        &nbsp;<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-undo"></i> &nbsp; Undo</button>
      </div>
    {!! Form::close() !!}
  </div>
@endif
