@extends('layouts.backend')
@section('title', 'Edit Event')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Event
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{route('admin.events.index')}}"><i class="fa fa-calendar"></i> All Events</a></li>
                <li class="active"><i class="fa fa-pencil"></i> Edit Event</li>
            </ol>
        </section>
        <div class="content">
            <div class="row">
                {!! Form::model($event, [
                  'method' => 'PUT',
                  'route' => ['admin.events.update', $event->id],
                  'id' => 'event-form'
                  ]) !!}
                @include('snippets.backend.events.form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        var simplemde1 = new SimpleMDE({ element: $("#excerpt")[0] });

        var simplemde2 = new SimpleMDE({ element: $("#body")[0] });
    </script>
@endsection