<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Page;
use App\Home;

class MenuController extends BackendController
{
  public function index()
  {
      //
      $menus = Menu::all();
      return view('admin.menus.index', compact('menus'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  // public function create()
  // {
  //     //
  //     return view('admin.menus.create');
  // }

  public function ajax_get_items() {

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
      $data = $request->all();
      $menu = Menu::create($data);
      return redirect(route('admin.menus.index'))->with('message', 'New menu successfully added');
  }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      $menu = Menu::findOrFail($id);
      $data = $menu->data;
      eval('$array = '.$data.';');
      dd($array);

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
      $menu = Menu::findOrFail($id);
      $pages = Page::all();
      return view('admin.menus.edit', compact('menu', 'pages'));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
      $data = $request->all();
      // dd($data);
      $menu = Menu::findOrFail($id);
      $menu->update($data);
      return redirect(route('admin.menus.index'))->with('message', 'Menu successfully updated');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      $menu = Menu::findOrFail($id);
      //deassociate all connected pages before removing the menu
      foreach ($menu->pages as $page) {
        $page->menu_id = null;
        $page->save();
      }
      $home = Home::first();
      if (isset($home->top_menu_id) && $home->top_menu_id == $menu->id) {
        $home->top_menu_id = null;
      }
      if (isset($home->footer_menu_id) && $home->footer_menu_id == $menu->id) {
        $home->footer_menu_id = null;
      }
      $home->save();
      $menu->delete();
      return redirect(route('admin.menus.index'))->with('message', 'Menu successfully deleted');

  }

  public function data(Request $request) {
    $word = $request->word;
    $pages = Page::where('title', 'LIKE', '%'.$word.'%')->get();
    return view('admin.menus.ajax', compact('pages'));
  }
}
