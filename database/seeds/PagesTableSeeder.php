<?php

use Illuminate\Database\Seeder;


class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //reset table
      DB::table('pages')->truncate();

      //Generate new data
      $root = App\Page::create([
        'title' => 'Root Page',
        'slug' => 'root-page',
        'body' => 'text',
        'excerpt' => 'text'
      ]);

      $child1 = $root->children()->create([
        'title' => 'child level 1',
        'slug' => 'child-1',
        'body' => 'text',
        'excerpt' => 'text'
      ]);

      $child2 = $child1->children()->create([
        'title' => 'child level 2',
        'slug' => 'child-2',
        'body' => 'text',
        'excerpt' => 'text'
      ]);
    }
}
