@extends('layouts.pages')
@section('title', 'Events')
@section('content')

    <button class="past-events-link">
        <a href="{{url('/past-events')}}">
            ALL PAST EVENTS
        </a>
    </button>

    {{--@if(count($eventsAMonthAgo) > 0)--}}
        {{--<div class="row"> <!-- START event banner -->--}}
        {{--<button class="past-events-link btn">--}}
            {{--<a href="{{url('/past-events')}}">--}}
                {{--EVENTS 1 MONTH AGO--}}
            {{--</a>--}}
        {{--</button>--}}
        {{--</div>--}}
    {{--@endif--}}

    <section class="event-banner" data-speed="6">
        <div class="row">
            <div class="col-md-12">
                <h1>UPCOMING EVENTS</h1>
            </div>
        </div>
    </section>

    @include('snippets.backend.events.event-thumbnails')

@endsection
