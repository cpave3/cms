<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Carbon\Carbon;
use App\User;

class BlogController extends Controller
{
    //
    protected $limit = 5;

    public function index() {
      //Show all published blog posts
      $posts = Post::with('author')
                      ->latestFirst()
                      ->published()
                      ->paginate($this->limit);
      return view('blog.index', compact('posts'));

    }

    public function single($slug) {
      //display an individual blog post
      $post = Post::published()->where('slug', '=', $slug)->firstOrFail();
      $post->increment('view_count');
      return view('blog.single', compact('post'));
    }

    public function category($slug) {
      //Data for the category view
      $posts = Category::where('slug', $slug)->firstOrFail()->posts()->published()->latestFirst()->paginate();
      $categoryName = Category::where('slug', $slug)->first()->title;
      return view('blog.index', compact('posts', 'categoryName'));
    }

    public function author($slug) {
      //Data for the category view
      $posts = User::where('slug', $slug)->firstOrFail()->posts()->published()->latestFirst()->paginate();
      return view('blog.index', compact('posts'));
    }
}
