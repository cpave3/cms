@extends('layouts.backend')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit User
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-user"></i> Users</li>
                <li class="active"><i class="fa fa-pencil"></i> Edit User Details</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body ">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.users.update', $user->id) }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('put') }}

                                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" autofocus>

                                                            @if ($errors->has('name'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    @if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Super Admin' )
                                                        <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                                                            <label for="role_id" class="col-md-4 control-label">Role</label>

                                                            <div class="col-md-6">
                                                                <select id="role_id" class="form-control" name="role_id">
                                                                    @foreach($roles as $role)
                                                                        @if ($role->name == 'Super Admin')
                                                                            @if(Auth::user()->role->name == 'Super Admin')
                                                                                <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                                                                            @else

                                                                            @endif
                                                                        @else
                                                                            @if ($user->role_id == $role->id)
                                                                                <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                                                                            @else
                                                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                                            @endif
                                                                        @endif

                                                                    @endforeach
                                                                </select>

                                                                @if ($errors->has('role_id'))
                                                                    <span class="help-block">
                                                                    <strong>{{ $errors->first('role_id') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endif


                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <a class="btn bg-gray-light" href="{{ url('admin/users') }}">Cancel</a>
                                                            <button type="submit" class="btn btn-primary">
                                                                Update
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
