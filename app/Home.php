<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    //
    protected $fillable = [
        'hero',
        'hero_text',
        'red1_text',
        'red1_link',
        'square1_text',
        'square1_link',
        'square2_text',
        'square2_link',
        'square3_text',
        'square3_link',
        'top_menu_id',
        'footer_menu_id',
        'footer_contact'
    ];

    public function footerMenu() {
      return $this->belongsTo('App\Menu', 'footer_menu_id');
    }

    public function headerMenu() {
      return $this->belongsTo('App\Menu', 'top_menu_id');
    }
}
