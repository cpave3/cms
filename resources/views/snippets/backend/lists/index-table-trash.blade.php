<table class="table table-bordered">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="200">Title</td>
      <td width="120">Members</td>
      <td>Description</td>
      <td width="200">Latest Subscription</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($subscriptions as $subscription)
      <tr>
        <td>
        {!! Form::open(['method' => 'PUT', 'action' => ['Backend\ListController@restore', $subscription->id], 'class' => 'form-inline']) !!}
          <button type="submit" class="btn btn-xs btn-success">
            <i class="fa fa-undo"></i>
          </button>
        {!! Form::close() !!}
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.lists.force-destroy', $subscription->id], 'class' => 'form-inline']) !!}
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-times"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$subscription->name}}</td>
        <td>{{$subscription->subscribers->count()}}</td>
        <td>{{$subscription->description}}</td>
        <td>PUT LATEST SUB DEETS HERE</td>
      </tr>
    @endforeach
  </tbody>
</table>
