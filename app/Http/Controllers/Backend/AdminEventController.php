<?php

namespace App\Http\Controllers\Backend;

use App\Event;
use App\Http\Requests\EventRequest;
use Illuminate\Http\Request;

class AdminEventController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$events = Event::all();
        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Event $event)
    {
		return view('admin.events.create', compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
      Event::createEvent($request);
  		return redirect(route('admin.events.index'))->with('success', 'A new Event was successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
		$event = Event::find($event->id);
		return view('admin.events.edit', compact('event') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
		Event::updateEvent($request, $event);
		return redirect(route('admin.events.index'))->with('success', 'An event was successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($event)
    {
		Event::destroy($event);
		return redirect(route('admin.events.index'))->with('success', 'An event was successfully deleted');
    }
}
