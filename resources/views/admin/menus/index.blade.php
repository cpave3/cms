@extends('layouts.backend')

@section('title', 'Menu Builder')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Menus
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-envelope"></i> Menu Builder</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                <div class="pull-left">

                  {!! Form::open([
                    'method' => 'POST',
                    'route' => 'admin.menus.store',
                    'class' => 'form-inline'
                    ]) !!}
                    <input type="text" name="name" class="form-control" placeholder="Enter new menu name" required="required" maxlength="128">
                    <button type="submit" class="btn btn-success" name="submit">Create &nbsp;<i class="fa fa-plus"></i></button>
                  {!! Form::close() !!}


                  {{-- <a href="{{route('admin.menus.create')}}" class="btn btn-success">Add New Menu &nbsp;<i class="fa fa-plus"></i></a> --}}

                </div>
                <div class="pull-right" style="padding: 7px 0;">
                  {{-- @include('snippets.backend.blog.links') --}}
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body ">
                @include('snippets.backend.message')
                @include('snippets.backend.blog.message')
                @if (!$menus->count())
                  <div class="alert alert-warning">
                    <strong>No Records Found</strong>
                  </div>
                @else
                  @include('snippets.backend.menus.index-table')
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('ul.pagination').addClass('no-margin pagination-sm');
  </script>
@endsection
