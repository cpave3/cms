<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class EventRequest extends FormRequest
{
	/**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(Request $request)
	{
		if ($request->isMethod('post') || $request->isMethod('put')) {
			return [
				  'title' => 'required|max:191',
				  'event_date' => 'required|date_format:Y-m-d H:i:s|after:yesterday',
				  'location' => 'required|max:191',
				  'excerpt' => 'required|max:255',
				  'body' => 'required'
			];
		}
	}

	public function messages()
	{
		return [
			  'event_date.required' => 'Please enter an event date',
			  'event_date.after' => 'The date cannot be before ' . date("d-m-y"),
		];
	}
}
