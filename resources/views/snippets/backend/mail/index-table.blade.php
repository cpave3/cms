<table class="table table-bordered">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="200">Title</td>
      <td width="120">Linked Lists</td>
      <td>Description</td>
      <td width="200">Latest Subscription</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($mails as $mail)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.mail.destroy', $mail->id]]) !!}
        <a href="{{route('admin.mail.edit', $mail->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$mail->name}}</td>
        <td>{{$mail->subscriptions->count()}}</td>
        <td>{{$mail->description}}</td>
        <td>
          {!! Form::open(['method' => 'POST', 'route' => ['admin.mail.send', $mail->id]]) !!}
          <button type="submit" class="btn btn-xs btn-success">
            <i class="fa fa-send"></i>
          </button>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
