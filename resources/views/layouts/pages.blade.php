<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <!--DEV BS-->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">-->
    <link rel="stylesheet" href="/css/frontend.css">
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <script type="text/javascript" charset="utf-8" src="/js/jquery.min.js?v=3.1.1"></script>
    <script type="text/javascript" charset="utf-8" src="/js/bootstrap.min.js?v=3.3.7"></script>
    <script type="text/javascript" charset="utf-8" src="/js/scripts.js"></script>
    <script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script>

 <title>@yield('title', 'QAI')</title>
</head>
<body>
  <header role="banner" class="">
      <div class="container">
          <div class="row logo-row">
              <div class="col-sm-12">
                  <a href="{{url('/')}}">
                    <img src="/img/qai.svg" alt="QAI Logo"> <!--HIDE ON MOBILE-->
                  </a>

                  <div class="pull-right search-container">
                      {!! Form::open(['route' => 'search']) !!}

                      {!! Form::text('query') !!}
                      {!! Form::submit('Search') !!}

                      {!! Form::close() !!}
                  </div>
              </div><!--col-->
          </div><!--row-->
          <div class="row menu-toggle">
              <div class="col-sm-12 pull-right">
                  <a href="#" class="btn btn-default btn-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                  </a>
              </div><!--col-->
          </div><!--row-->
      </div><!--container-->
  </header><!--header-->

  <nav role="navigation" class="collapse navbar-collapse" id="navbar">
      <div class="container">
          <div class="row">
              <div class="col-sm-12">
                @php
                  $menu = App\Home::first();
                  $menu = App\Menu::findOrFail($menu->top_menu_id); //TOP MENU
                  eval('$m = '.$menu->data.';');
                @endphp
                  <ul class="nav navbar-nav">
                    @foreach ($m as $root)
                      @if ($root[0] == "x1" || $root[0] == "x2" || $root[0] == "x3")
                        @php
                          switch ($root[0]) {
                            case "x1":
                              echo "<li class=\"menu-item dropdown\"><a href=\"".url('/')."\">Home</a>";
                              break;
                            case "x2":
                              echo "<li class=\"menu-item dropdown\"><a href=\"".url('/blog')."\">Blog</a>";
                              break;
                            case "x3":
                              echo "<li class=\"menu-item dropdown\"><a href=\"".url('/events')."\">Events</a>";
                              break;
                          }
                        @endphp
                      @else
                        @php
                          $p = App\Page::find($root[0])
                        @endphp
                        @if (isset($p))
                          <li class="menu-item dropdown"><a href="{{url('/').'/'.$p->path}}">{{$root[1]}}</a>
                        @endif
                      @endif

                        @if (!empty($root[2]))
                          <ul class="dropdown-menu">
                            @foreach ($root[2] as $child1)

                              @if ($child1[0] == "x1" || $child1[0] == "x2" || $child1[0] == "x3")
                                @php
                                  switch ($child1[0]) {
                                    case 'x1':
                                      echo "<li class=\"dropdown-submenu\"><a href=\"".url('/')."\">Home</a>";
                                      break;
                                    case 'x2':
                                      echo "<li class=\"dropdown-submenu\"><a href=\"".url('/blog')."\">Blog</a>";
                                      break;
                                    case 'x3':
                                      echo "<li class=\"dropdown-submenu\"><a href=\"".url('/events')."\">Events</a>";
                                      break;
                                  }
                                @endphp
                              @else
                                @php
                                  $p = App\Page::find($child1[0])
                                @endphp
                                @if (isset($p))
                                  <li class="dropdown-submenu"><a href="{{url('/').'/'.$p->path}}">{{$child1[1]}}</a>
                                @endif
                              @endif

                                @if ($child1[2])
                                  <ul class="dropdown-menu">
                                    @foreach ($child1[2] as $child2)

                                      @if ($child2[0] == "x1" || $child2[0] == "x2" || $child2[0] == "x3")
                                        @php
                                          switch ($child2[0]) {
                                            case 'x1':
                                              echo "<li class=\"dropdown-submenu\"><a href=\"".url('/')."\">Home</a>";
                                              break;
                                            case 'x2':
                                              echo "<li class=\"dropdown-submenu\"><a href=\"".url('/blog')."\">Blog</a>";
                                              break;
                                            case 'x3':
                                              echo "<li class=\"dropdown-submenu\"><a href=\"".url('/events')."\">Events</a>";
                                              break;
                                          }
                                        @endphp
                                      @else
                                        @php
                                          $p = App\Page::find($child2[0])
                                        @endphp
                                        @if (isset($p))
                                          <li class="dropdown-submenu"><a href="{{url('/').'/'.$p->path}}">{{$child2[1]}}</a>
                                        @endif
                                      @endif
                                    @endforeach
                                  </ul>
                                @endif
                              </li>
                            @endforeach
                            {{-- <li class="dropdown-submenu"><a href="#">test</a></li> --}}
                          </ul>
                        @endif
                      </li>
                      {{-- <li>{{$root[1]}}</li> --}}
                    @endforeach
                  </ul>
              </div>
          </div>
      </div>
  </nav><!--nav-->


  {{-- <br> --}}
  {{-- move content down --}}
  @yield('content')

  @php
    $home = App\Home::first();
    $footerMenu = App\Menu::findOrFail($home->footer_menu_id);
    eval('$m = '.$footerMenu->data.';');
  @endphp

  <footer>
      <div class="container">
          <div class="row">

             <div class="col-sm-4 xs-hide">
                <div class="footer-menu">
                  @if (isset($m))
                    <ul>
                        @foreach ($m as $root)

                          @if ($root[0] == "x1" || $root[0] == "x2" || $root[0] == "x3")
                            @php
                              switch ($root[0]) {
                                case 'x1':
                                  echo "<li>
                                    <a href=\"".url('/')."\">Home</a>
                                    <ul>";
                                  break;
                                case 'x2':
                                echo "<li>
                                  <a href=\"".url('/blog')."\">Blog</a>
                                  <ul>";
                                  break;
                                case 'x3':
                                echo "<li>
                                  <a href=\"".url('/events')."\">Events</a>
                                  <ul>";
                                  break;
                              }
                            @endphp
                          @else
                            @php $p = App\Page::find($root[0]); @endphp
                            <li>
                              <a href="{{$p->path}}">{{$root[1]}}</a>
                              <ul>
                          @endif
                              @foreach ($root[2] as $child1)

                                @if ($child1[0] == "x1" || $child1[0] == "x2" || $child1[0] == "x3")
                                  @php
                                    switch ($child1[0]) {
                                      case 'x1':
                                        echo "<li>
                                          <a href=\"".url('/')."\">Home</a>
                                          <ul>";
                                        break;
                                      case 'x2':
                                      echo "<li>
                                        <a href=\"".url('/blog')."\">Blog</a>
                                        <ul>";
                                        break;
                                      case 'x3':
                                      echo "<li>
                                        <a href=\"".url('/events')."\">Events</a>
                                        <ul>";
                                        break;
                                    }
                                  @endphp
                                @else
                                  @php $p = App\Page::find($child1[0]); @endphp
                                  <li>
                                    <a href="{{$p->path}}">{{$child1[1]}}</a>
                                    <ul>
                                @endif

                                    @foreach ($child1[2] as $child2)

                                      @if ($child2[0] == "x1" || $child2[0] == "x2" || $child2[0] == "x3")
                                        @php
                                          switch ($child2[0]) {
                                            case 'x1':
                                              echo "<li>
                                                <a href=\"".url('/')."\">Home</a>
                                                <ul>";
                                              break;
                                            case 'x2':
                                            echo "<li>
                                              <a href=\"".url('/blog')."\">Blog</a>
                                              <ul>";
                                              break;
                                            case 'x3':
                                            echo "<li>
                                              <a href=\"".url('/events')."\">Events</a>
                                              <ul>";
                                              break;
                                          }
                                        @endphp
                                      @else
                                        @php $p = App\Page::find($child2[0]); @endphp
                                        <li>
                                          <a href="{{$p->path}}">{{$child2[1]}}</a>
                                          <ul>
                                      @endif

                                    @endforeach
                                  </ul>
                                </li>
                              @endforeach
                            </ul>
                          </li>
                        @endforeach
                    </ul>
                  @endif
                </div>
              </div><!--col-->

              @php
                $h = App\Home::first();
              @endphp
             <div class="col-sm-4 xs-hide">
                  {{-- <h2>Get in Touch</h2>
                  <ul>
                      <li><i class="fa fa-phone" aria-hidden="true"></i> (07) 3844 4200</li>
                      <li><i class="fa fa-fax" aria-hidden="true"></i> (07) 3844 4220</li>
                      <li><i class="fa fa-phone-square" aria-hidden="true"></i> 1800 130 582</li>
                      <li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:info@qai.org.au">info@qai.org.au</a></li>
                      <li><i class="fa fa-globe" aria-hidden="true"></i> <a href="#">qai.org.au</a></li>
                  </ul> --}}
                  {!! $h->footer_contact !!}
              </div><!--col-->

             @include('snippets.backend.subscribe.subscribe-form')

          </div><!--row-->
      </div><!--container-->
  </footer><!--contact info-->

  {{-- Google Translate Widget --}}
  <div id="google_translate_element"></div>
  <script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
  }
  </script>
  <script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script>
</body>
</html>
