@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.categories.index')}}"><i class="fa fa-folder"></i> Categories</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Edit Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {!! Form::model($category, [
            'method' => 'PUT',
            'route' => ['admin.categories.update', $category->id],
            'files' => TRUE,
            'id' => 'category-form'
            ]) !!}
          @include('snippets.backend.categories.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.categories')
@endsection
