<script type="text/javascript">
  $('#title').on('blur', function() {
    var theTitle = this.value.toLowerCase().trim(),
        slugInput = $('#slug'),
        theSlug = theTitle.replace(/&/g, '-and-').replace(/[^a-z0-9-]+/g, '-').replace(/\-\-+/g, '-').replace(/^-+|-+$/g);
    if (slugInput.val() == "") {
      slugInput.val(theSlug);
    }
  });

  $("#slug").on("keyup", function() {
    this.value = this.value
    .toLowerCase()
    .trim()
    .replace(/&/g, '-and-')
    // .replace(/[^\w\s&-]/g, '')
    .replace(/[^a-z0-9-]+/g, '-')
    .replace(/\-\-+/g, '-')
    .replace(/^-+|-+$/g, '-');
  });

</script>
