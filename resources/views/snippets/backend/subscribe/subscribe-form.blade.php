<div class="col-sm-4">
    <h4>Sign up for our newsletter to stay up-to-date with the latest news</h4>
    <form role="form" method="POST" action="/">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="newsletter-name" class="sr-only">Name</label>

                <input type="text" class="form-control" placeholder="Name..." id="newsletter-name" name="name" value="{{ old('name') }}">

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
        </div><!--form group-->

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="newsletter-email" class="sr-only">Email Address</label>

            <input type="email" class="form-control" placeholder="Email Address..." id="newsletter-email" name="email" value="{{ old('newsletter-email') }}">

            @if ($errors->has('email'))
                <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
            @endif
        </div><!--form group-->

        <div class="form-group">
            <input type="submit" value="Subscribe!" class="btn btn-success btn-block">
        </div><!--form group-->
    </form>

    @if(isset($subscribed) && $subscribed == true)
        <p>Thank you for subscribing to QAI!</p>
    @endif
</div><!--col-->
