<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->truncate();

        DB::table('categories')->insert([
          [
            'title' => 'Uncategorised',
            'slug' => 'uncategorised'
          ],
          [
            'title' => 'Sample Category 1',
            'slug' => 'sample-category-1'
          ],
          [
            'title' => 'Sample Category 2',
            'slug' => 'sample-category-2'
          ],
          [
            'title' => 'Sample Category 3',
            'slug' => 'sample-category-3'
          ],
        ]);

        //Update posts
        for ($post_id=1; $post_id <= 20; $post_id++) {
          $category_id = rand(1,3);
          DB::table('posts')->where('id', $post_id)->update(['category_id' => $category_id]);
        }
    }
}
