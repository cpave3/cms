@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body ">
                    <h3>Welcome, {{Auth::user()->name}}</h3>
                    <p class="lead text-muted">Quicklinks</p>
                    <ul>
                      <li><a href="{{route('admin.blog.create')}}">New Blog Post</a></li>
                      <li><a href="{{route('admin.pages.create')}}">New Page</a></li>
                      <li><a href="{{route('admin.menus.index')}}">Menu Builder</a></li>
                      <li><a href="{{route('admin.events.create')}}">New Event</a></li>
                    </ul>

                    {{-- <h4>Get started</h4> --}}
                    {{-- <p><a href="{{route('admin.blog.create')}}" class="btn btn-primary">Write your first blog post</a> </p> --}}
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
