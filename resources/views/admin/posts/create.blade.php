@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Post
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.blog.index')}}"><i class="fa fa-pencil"></i> Blog Posts</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Create New Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-sm-12">
            @include('snippets.backend.message')
          </div>
          {!! Form::model($post, [
            'method' => 'POST',
            'route' => 'admin.blog.store',
            'files' => TRUE,
            'id' => 'post-form'
            ]) !!}
          @include('snippets.backend.blog.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.blog')
@endsection
