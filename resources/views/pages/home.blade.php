@extends('layouts.pages')
@section('title', 'Queensland Advocacy Incorporated')
@section('content')
    @if(isset($subscribed) && $subscribed == true)
        <div class="subscribe-success"><p><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Thank You for Subscribing!</p></div>
    @endif
  <section id="hero" data-type="" data-speed="6" @if ($home->hero) style="background: url({{'/uploads/'.$home->hero}});" @endif> <!--scrolling BG-->
      <div class="title-container">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              {!! $home->hero_text !!}
            </div>
          </div>
        </div>
      </div>
  </section><!--Hero Image-->

  <section id="hero-content">
      <div class="inner-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                     <p class="lead">{{$home->red1_text}}</p>
                     <a href="{{$home->red1_link}}" class="btn-cta-red btn btn-cta btn-lg">Learn more</a>
                 </div>
             </div>
        </div>

     </div>
  </section>

  <section id="services">
      <div class="container">
          <div class="row">
              <div class="col-sm-4 service">
                  <div>
                    {!! $home->square1_text !!}
                  </div>
                  <p><a href="{{$home->square1_link}}" class="btn btn-default btn-lg btn-cta-red">Learn More...</a></p>
              </div><!--service-->

              <div class="col-sm-4 service">
                <div>
                  {!! $home->square2_text !!}
                </div>
                <p><a href="{{$home->square2_link}}" class="btn btn-default btn-lg btn-cta-red">Learn More...</a></p>
              </div><!--service-->

              <div class="col-sm-4 service">
                <div>
                  {!! $home->square3_text !!}
                </div>
                <p><a href="{{$home->square3_link}}" class="btn btn-default btn-lg btn-cta-red">Learn More...</a></p>
              </div><!--service-->

          </div><!--Row-->
      </div><!--Container-->
  </section><!--Services-->

  <section id="news">
     <div class="container">
         <div class="row">
             <div class="col-sm-8">
               @php
                 $blog = App\Post::published()->orderBy('published_at', 'desc')->first();
               @endphp
                 <h2>Latest News</h2>
                 <article class="news-excerpt">
                     <small>{{$blog->created_at->diffForHumans()}} &#124; {{$blog->category->title}}</small>
                     <h3>{{$blog->title}}</h3>
                     <div>
                       {!! $blog->excerpt !!}
                     </div>
                     <br>
                     <a href="/blog/{{$blog->slug}}" class="readmore">Read More</a>
                 </article>

             </div> <!--col-->
             <div class="col-sm-4">
                 <h2>Upcoming Events</h2>
                 <ul class="events">
                     @if(isset($noEvents) && $noEvents)
                         <p class="home-page-no-events-msg">There are no Upcoming Events</p>
                     @else
                         @if(isset($upcomingEvents))
                             @foreach($upcomingEvents as $event)
                                 <a href="{{route('event.index', $event->id)}}">
                                     <li>
                                         <h3>{{$event->title}}</h3>
                                         <span>{{$event::formatdate($event->event_date)}}</span>
                                     </li>
                                 </a>
                             @endforeach
                         @endif
                     @endif
                 </ul>
             </div><!--col-->
         </div><!--row-->
     </div><!--container-->
  </section><!--News-->

{{--  @include('snippets.backend.modal.subscribe-success-modal')

  @if($subscribed == true)
      <script type="text/javascript">


          $("#confirmationModal").modal({
              backdrop: "static",
              keyboard: false
          });

      </script>
  @endif--}}

@endsection
