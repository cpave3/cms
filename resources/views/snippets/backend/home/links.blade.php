@php
  $links = [];
@endphp
@foreach ($statusList as $key => $value)
  @if ($value)
    @php
      $selected = Request::get('status') == $key ? 'selected-status' : '';
      $links[] = "<a class=\"{$selected}\" href=\"?status=$key\"> ".ucwords($key)." ({$value})</a>";
    @endphp
  @endif
@endforeach
{!! implode(' | ', $links) !!}
