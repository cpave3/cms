<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use GrahamCampbell\Markdown\Facades\Markdown;

/**
 * App\Post
 *
 * @property-read \App\User $author
 * @property-read \App\Category $category
 * @property-read mixed $body_html
 * @property-read mixed $date
 * @property-read mixed $excerpt_html
 * @property-read mixed $image_url
 * @property-read mixed $publication_label
 * @property-read mixed $thumb_url
 * @property-write mixed $published_at
 * @method static \Illuminate\Database\Query\Builder|\App\Post latestFirst()
 * @method static \Illuminate\Database\Query\Builder|\App\Post popular()
 * @method static \Illuminate\Database\Query\Builder|\App\Post published()
 * @mixin \Eloquent
 */
class Post extends Model
{

    public function attachments() {
      return $this->belongsToMany('App\Upload', 'post_upload');
    }

    //Attributes
    use SoftDeletes;
    protected $dates = ['published_at'];

    protected $fillable = [
      'title',
      'body',
      'excerpt',
      'view_count',
      'slug',
      'published_at',
      'category_id',
      'user_id',
      'image'
    ];

    //Mutator
    public function setPublishedAtAttribute($value) {
      $this->attributes['published_at'] = $value ?: NULL;
    }

    //ACCESSORS
    public function getImageUrlAttribute($value) {
      $imageUrl = "";
      $directory = config('cms.image.directory');
      if (!is_null($this->image)) {
        $imagePath = public_path() . "/$directory/" . $this->image;
        if (file_exists($imagePath)) {
          $imageUrl = asset("$directory/" . $this->image);
        }
      }
      return $imageUrl;
    }

    public function getThumbUrlAttribute($value) {
      $imageUrl = "";
      $directory = config('cms.image.directory');
      if (!is_null($this->image)) {
        $imagePath = public_path() . "/$directory/" . $this->image;
        if (file_exists($imagePath)) {
          $thumb = str_replace('.', '_thumb.', $this->image);
          $imageUrl = asset("$directory/" . $thumb);
        }
      }
      return $imageUrl;
    }

    public function getDateAttribute($value) {
      return is_null($this->published_at) ? '' : $this->published_at->diffForHumans();
    }

    public function getBodyHtmlAttribute ($value) {
      return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
    }

    public function getExcerptHtmlAttribute ($value) {
      return $this->excerpt ? Markdown::convertToHtml(e($this->excerpt)) : NULL;
    }

    public function dateFormatted($showTimes = false) {
      $format = 'd/m/Y';
      if ($showTimes) {$format = $format . " H:i:s";}
      if ($this->published_at) {
        return $this->published_at->format($format);
      } else {
        return $this->created_at->format($format);
      }
    }

    public function getPublicationLabelAttribute() {
      if ($this->deleted_at) {
          return "<span class='label label-danger'>Trashed</span>";
      } elseif (!$this->published_at) {
          return "<span class='label label-warning'>Draft</span>";
      } elseif ($this->published_at && $this->published_at->isFuture() ) {
          return "<span class='label label-info'>Scheduled</span>";
      } else {
          return "<span class='label label-success'>Published</span>";
      }
    }

    //RELATIONSHIPS
    public function author() {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function category() {
      return $this->belongsTo('App\Category');
    }

    //SCOPES
    public function scopeLatestFirst($query) {
      return $query->orderBy('published_at', 'desc');
    }

    public function scopePublished($query) {
      return $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeDraft($query) {
      return $query->where('published_at', '=', null);
    }

    public function scopeScheduled($query) {
      return $query->where('published_at', '>', Carbon::now());
    }

    public function scopePopular($query) {
      return $query->orderBy('view_count', 'desc');
    }

}
