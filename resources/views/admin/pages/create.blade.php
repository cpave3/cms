@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Page
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.pages.index')}}"><i class="fa fa-file"></i> All Pages</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Create New Page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-sm-12">
            @include('snippets.backend.message')
          </div>
          {!! Form::model($page, [
            'method' => 'POST',
            'route' => 'admin.pages.store',
            'files' => TRUE,
            'id' => 'pages-form'
            ]) !!}
          @include('snippets.backend.pages.form')
          {{-- {!! Form::close() !!} --}}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.pages')
@endsection
