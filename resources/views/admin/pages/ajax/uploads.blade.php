@if ($page->attachments->count() > 0)
  <ul>
    <style scoped>
      form {
        display: inline-block;
      }

      .btn-delete {
        background-color: transparent;
        border: none;
        color: red;
      }

      .btn-info {
        background-color: transparent;
        border: none;
        color: blue;
      }

      .btn-info:hover {
        background-color: transparent;
        border: none;
        color: blue;
      }
    </style>
    @foreach ($page->attachments as $attachment)
      <li><a href="{{$attachment->path}}" target="_blank">{{$attachment->name}}</a> - {{$attachment->size}} |
        {!! Form::open(['method' => 'DELETE', 'class' => 'ajax-delete-form', 'route' => ['admin.uploads.destroy', $attachment->id]]) !!}
        <button type="submit" class="btn-delete">
          Delete
        </button>
        {!! Form::close() !!}

        |
            {!! Form::open(['method' => 'PUT', 'class' => 'ajax-hide-form', 'route' => ['admin.uploads.hideToggle', $attachment->id]]) !!}
            <button type="submit" class="btn-info">
              @if ($attachment->hidden == 1)
                Unhide
              @elseif ($attachment->hidden == 0)
                Hide
              @endif
            </button>
            {!! Form::close() !!}
    @endforeach
  </ul>
@else
  <span>No Attachments to show</span>
@endif
