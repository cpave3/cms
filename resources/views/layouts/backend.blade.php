<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title', 'QAI')</title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/css/skins/_all-skins.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  {{-- SimpleMDE --}}
  <link rel="stylesheet" href="/plugins/simple-mde/simplemde.min.css">
  <link rel="stylesheet" href="/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css">

  {{-- datetimepicker --}}
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">

  <link rel="stylesheet" href="/backend/css/custom.css">

  {{-- Data tables JQ --}}
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

  <style media="screen">
  .mce-ico.mce-i-fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=fgwddhw01hh1ipgx6xc4stpl2sziyj1j6bq8vjzmver8cn5n"></script>

  <script>
    tinymce.init({
      selector:'.tinymce',
      plugins: 'autosave wordcount code textcolor preview media image link table lists',
      toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table | fontsizeselect restoredraft code forecolor backcolor preview media image link gdoc',
      setup: function (editor) {
        editor.addButton('gdoc', {
          text: 'PDF Viewer',
          icon: 'fa fa-file-pdf-o',
          onclick: function () {
              var url = prompt("Please enter PDF URL");
              if (url == null || url == "") {
                  alert("No URL entered");
              } else {
                  editor.insertContent('<iframe width="100%" height="900px" src="https://docs.google.com/viewer?key={{env('GOOGLE_KEY')}}&embedded=true&url='+url+'"></iframe>');
              }
          }
        });
      }
      // menubar: 'code view insert'
    });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('snippets.backend.header')

  <!-- Left side column. contains the logo and sidebar -->
  @include('snippets.backend.sidebar')

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- /.content-wrapper -->

  <div class="modal fade" id="upload-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Upload Image</h4>
        </div>
        <div class="modal-body">
          {!! Form::open([
            'method' => 'POST',
            'route' => 'admin.uploads.store',
            'files' => TRUE,
            'id' => 'ajax-upload-form'
          ]) !!}
          {!! Form::hidden('type', 2) !!}
            <div class="form-group">
              {!! Form::label('name')!!}
              {!! Form::text('name', null, ['class' => 'form-control'])!!}
            </div>
            <div class="form-group">
              {!! Form::label('alt')!!}
              {!! Form::text('alt', null, ['class' => 'form-control'])!!}
            </div>
            <div class="form-group">
              {!! Form::label('image')!!}
              {!! Form::file('image')!!}
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        {!! Form::close() !!}

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  {{-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.1
    </div>
    <strong>Developed for Queensland Advocacy Incorporated</strong>
  </footer> --}}

</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/js/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/js/app.min.js"></script>
{{-- SimpleMDE Script --}}
{{-- <script type="text/javascript" src="/plugins/simple-mde/simplemde.min.js"></script> --}}
<script type="text/javascript" src="/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js" charset="utf-8"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js" charset="utf-8"></script>
<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.1/jquery.form.min.js" integrity="sha384-tIwI8+qJdZBtYYCKwRkjxBGQVZS3gGozr3CtI+5JF/oL1JmPEHzCEnIKbDbLTCer" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js" charset="utf-8"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#data-table').DataTable({
    "bStateSave": true
  });
});
</script>

<script type="text/javascript">

function resetForm(formid) {
  $(':input','#'+formid) .not(':button, :submit, :reset, :hidden') .val('')
 .removeAttr('checked') .removeAttr('selected');
 }
    $(function() {
      var options = {
      url:     '{{route("admin.uploads.store")}}',
      type:     'POST',
      success: function(response) {
        // alert('submitted!');
        // console.log(response);
        resetForm('ajax-upload-form');
        $('#upload-modal').modal('hide');
      }
    };
    // pass options to ajaxForm
    $('#ajax-upload-form').ajaxForm(options);
     });
</script>
@yield('script')
</body>
</html>
