<script type="text/javascript">
  $('ul.pagination').addClass('no-margin pagination-sm');

  $('#title').on('blur', function() {
    var theTitle = this.value.toLowerCase().trim(),
        slugInput = $('#slug'),
        theSlug = theTitle.replace(/&/g, '-and-').replace(/[^a-z0-9-]+/g, '-').replace(/\-\-+/g, '-').replace(/^-+|-+$/g);
    if (slugInput.val() == "") {
      slugInput.val(theSlug);
    }
  });

  $("#slug").on("keyup", function() {
    this.value = this.value
    .toLowerCase()
    .trim()
    .replace(/&/g, '-and-')
    // .replace(/[^\w\s&-]/g, '')
    .replace(/[^a-z0-9-]+/g, '-')
    .replace(/\-\-+/g, '-')
    .replace(/^-+|-+$/g, '-');
  });

  dateNow = new Date();
  $("#published_at").datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    showClear: true,
    defaultDate: dateNow
  });

  $("#draft-btn").click(function(e) {
    e.preventDefault();
    $("#published_at").val("")
    $("#post-form").submit();
  });

  

</script>
