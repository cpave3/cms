@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Post
        @if (($post->published_at) && $post->published_at <= Carbon\Carbon::now())
          <small><a href="{{route('blog.single', $post->slug)}}">View Post</a></small>
        @endif
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.blog.index')}}"><i class="fa fa-pencil"></i> Blog Posts</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Edit Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-sm-12">
            @include('snippets.backend.message')
          </div>
          {!! Form::model($post, [
            'method' => 'PUT',
            'route' => ['admin.blog.update', $post->id],
            'files' => TRUE,
            'id' => 'post-form'
            ]) !!}
          @include('snippets.backend.blog.form')
          {{-- {!! Form::close() !!} --}}

        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.blog')
  <script type="text/javascript">
  function refreshAttachments() {
    $.ajax({
     method: "GET",
     url: "{{route('admin.uploads.ajax.attachments2', $post->id)}}",
   })
     .done(function( response ) {

       $("#attachments").html(response);
       $('.ajax-delete-form').on('submit', function() {
         return confirm('Are you sure you wish to permanatley delete this attachment?');
       });
       $('.ajax-delete-form').ajaxForm({
         success: function(res) {
             console.log(res);
             refreshAttachments();
         }
       });
     });
  }

  function disableUploadButton(id) {
   $button = $("#"+id);
   $button.prop('disabled', true);
   $button.toggleClass("btn-success");
   $button.children("span").text("Uploading...  ");
   $button.children("i").toggleClass("off")
   $button.children("i").toggleClass("spinner");
  }

  function enableUploadButton(id) {
   $button = $("#"+id);
   $button.prop('disabled', false);
   $button.toggleClass("btn-success");
   $button.children("span").text("Upload Attachment");
   $button.children("i").toggleClass("off")
   $button.children("i").toggleClass("spinner");
  }

  $(function() {
   var optionsStore = {
   url:     '{{route("admin.uploads.store")}}',
   type:     'POST',
   success: function(response) {
     resetForm('attachment-form');
     console.log("working");
     refreshAttachments();
     enableUploadButton("attachment-submit");

   }
  };

  //allow submissions of attachments via ajax
  $('#attachment-form').ajaxForm(optionsStore);
  });

  $('.ajax-delete-form').on('submit', function() {
   return confirm('Are you sure you wish to permanatley delete this attachment?');
  });
  // $(function() {
  // pass options to ajaxForm
  $('.ajax-delete-form').ajaxForm({
   success: function(res) {
       console.log(res);
       refreshAttachments();
   }
  });


  $('#attachment-submit').on('click', function() {
   disableUploadButton("attachment-submit");
   $("#attachment-form").submit();
  });

  // });
  // TODO: Needs serious refactoring
  </script>
@endsection
