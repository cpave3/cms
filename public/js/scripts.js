$(function () {
    
    //cache window obj
    var $window = $(window);
    
    //Parallax
    $('section[data-type="background"]').each(function () {
        var $bgobj = $(this); //assigning the object
        
        $window.scroll(function () {
            //scroll at var speed
            //yPos negative because going up
            var yPos = -($window.scrollTop() / $bgobj.data('speed'));
            
            //put together final bg position
            var coords = '50% ' + yPos + 'px';
            
            //move background
            $bgobj.css({ backgroundPosition: coords });
            
        }); //end window scroll
    });
});