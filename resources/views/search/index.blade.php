@extends('layouts.pages')
@section('title', 'Search Results')
@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
        <div class="search-result">
            <h1>Search results for "{{ $query }}"</h1>
            @if(count($results) == 0)
                <h2>No results found</h2>
            @else
                <h2>{{ count($results) }} Results</h2>
            @endif

            <ul>
                @foreach($results as $result)

                    @if($result->Type == "Pages")
                        <li>
                            <h3>{{ link_to_route('pages.show', $result->title, $result->path) }}</h3>
                            <p>{{ mb_strimwidth($result->excerpt, 0, 150, "...") }}</p>
                            <span><strong>Category: Page</strong></span>
                            <p>
                                <strong>Updated At: {{ Carbon\Carbon::parse($result->updated_at)->format('l, jS F Y') }}</strong>
                            </p>
                        </li>
                    @elseif($result->Type == "Posts")
                        <li>
                            <h3>{!! link_to_route('blog.single', $result->title, $result->slug) !!}</h3>
                            <p>{{ mb_strimwidth($result->excerpt, 0, 150, "...") }}</p>
                            <span><strong>Category: Blog Post</strong></span>
                            <p>
                                <strong>Updated At: {{ Carbon\Carbon::parse($result->updated_at)->format('l, jS F Y') }}</strong>
                            </p>
                        </li>
                    @elseif($result->Type == "Events")
                        <li>
                            <h3>{!! link_to_route('event.index', $result->title, $result->id) !!}</h3>
                            <p>{{ mb_strimwidth($result->excerpt, 0, 150, "...") }}</p>
                            <span><strong>Category: Event</strong></span>
                            <p>
                                <strong>Event Date: {{ Carbon\Carbon::parse($result->event_date)->format('l, jS F Y') }}</strong>
                            </p>
                        </li>
                    @endif

                @endforeach
            </ul>

           {{--{!! $results->setPath('search')->render() !!}--}}

           <div class="search-container">
               {!! Form::label('Search Again?')!!}
               {!! Form::open(['route' => 'search']) !!}

               {!! Form::label('Pages')!!}
               {!! Form::checkbox('type-pages', true, $pagesChecked) !!}

               {!! Form::label('Posts')!!}
               {!! Form::checkbox('type-posts', true, $postsChecked) !!}

               {!! Form::label('events')!!}
               {!! Form::checkbox('type-events', true, $eventsChecked) !!}

               {!! Form::text('query', $query) !!}
               {!! Form::submit('Search') !!}

               {!! Form::close() !!}
           </div>

        </div>
    </div>
  </div>
</div>

@endsection
