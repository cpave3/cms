<div class="col-md-10 col-md-offset-1">
    <article>
        <div class="event-thumbnail-container container">
            @if(count($events) == 0)
                <p class="zero-events-msg">{{$noEventsMsg}}</p>
            @else
                @foreach($events as $event)
                    <div class="col-md-4">
                        <section class="event-thumbnail">
                            <img src="img/header-bg.jpg" alt="event image">
                            <a href="{{route('event.index', $event->id)}}">
                                <h2>{{ $event->title }}</h2>
                            </a>
                            <p><i class="fa fa-calendar" aria-hidden="true"></i><strong>  When: {{ $event::formatdate($event->event_date) }}</strong></p>
                            <p><i class="fa fa-map-marker" aria-hidden="true"></i><strong>  Location: {{ $event->location }}</strong></p>
                            <p class="event-text">{{ $event->excerpt }}</p>
                            <div class="event-thumbnail-border"></div>
                        </section>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="col-md-4 col-md-offset-5">
            {{ $events->links() }}
        </div>
    </article>
</div>

{{-- <section id="hero-content"></section> --}}
