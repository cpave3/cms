<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\Http\Requests\PageRequest;
use Intervention\Image\Facades\Image;


class AdminPageController extends BackendController
{
  protected $uploadPath;

  public function __construct() {
    parent::__construct();
    $this->uploadPath = public_path(config('cms.image.directory'));
  }

 public function index(Request $request)
 {
     //
     $onlyTrashed = FALSE;
     if (($status = $request->get('status')) && $status == 'trash')
     {
       $pages = Page::onlyTrashed()->latest()->get();
       $pageCount = Page::onlyTrashed()->count();
       $onlyTrashed = TRUE;
     }
     elseif (($status = $request->get('status')) && $status == 'published')
     {
       $pages = Page::published()->latest()->get();
       $pageCount = Page::published()->count();
     }
     elseif (($status = $request->get('status')) && $status == 'scheduled')
     {
       $pages = Page::scheduled()->latest()->get();
       $pageCount = Page::scheduled()->count();
     }
     elseif (($status = $request->get('status')) && $status == 'draft')
     {
       $pages = Page::draft()->latest()->get();
       $pageCount = Page::draft()->count();
     }
     else
     {
       $pages = Page::get();
       $pageCount = Page::count();
     }

     $statusList = $this->statusList();
     return view('admin.pages.index', compact('pages', 'pageCount', 'onlyTrashed', 'statusList'));
 }

 private function statusList() {
   return [
     'all' => Page::count(),
     'published' => Page::published()->count(),
    //  'scheduled' => Page::scheduled()->count(),
     'draft' => Page::draft()->count(),
     'trash' => Page::onlyTrashed()->count(),
   ];
 }

 /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
 public function create(Page $page)
 {
     //
     $pages = Page::orderBy('lft', 'ASC')->get();
     $allPages = [];

     foreach ($pages as $p) {
       $allPages[$p->id] = $p->indented;
     }

     return view('admin.pages.create', compact('page', 'allPages'));
 }

 /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
 public function store(PageRequest $request)
 {
     //
     // $this->validate($request, [
     //
     // ]);

    //  return $request->all();

     $data = $this->handleRequest($request);

     $page = Page::create($data);
    //  $request->pages()->create($data);
     if (isset($data['parent']) && !empty($data['parent'])) {
       $parent_id = $data['parent'];
       $parent = Page::findOrFail($parent_id);
       $page->makeChildOf($parent);
     }

     return redirect(route('admin.pages.edit', $page->id))->with('message', 'Your page was successfully created');
 }

 private function handleRequest($request) {
   $data = $request->all();
   if ($request->hasFile('image')) {
     $image = $request->file('image');
     $fileName = $image->getClientOriginalName();

     $destination = $this->uploadPath;
     $successUploaded = $image->move($destination, $fileName);

     if ($successUploaded) {
       $extention = $image->getClientOriginalExtension();
       $thumbnail = str_replace(".{$extention}", "_thumb.{$extention}", $fileName);

       //scale to 250px wide
       $width = config('cms.image.thumbnail.width');
       $height = config('cms.image.thumbnail.height');
       Image::make($destination . '/' . $fileName)
       ->resize($width,$height)
       ->save($destination . '/' . $thumbnail);

       //crop to 250px x 175px
       // Image::make($destination . '/' . $fileName)
       // ->crop($width, $height)
       // ->save($destination . '/' . $thumbnail);
     }

     $data['image'] = $fileName;
   }
   return $data;
 }

 /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function show($id)
 {
     //
 }

 /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function edit($id)
 {
     //
     $pages = Page::orderBy('lft', 'ASC')->get();
     $allPages = [];

     foreach ($pages as $p) {
       $allPages[$p->id] = $p->indented;
     }

     $page = Page::findOrFail($id);
     return view('admin.pages.edit', compact('page', 'allPages'));
 }

 /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function update(PageRequest $request, $id)
 {
     //
     $page = Page::findOrFail($id);
     $data = $this->handleRequest($request);
     $oldImage = $page->image;
     $page->update($data);
     if (isset($data['parent']) && !empty($data['parent'])) {
       $parent_id = $data['parent'];
       $parent = Page::findOrFail($parent_id);
       $page->makeChildOf($parent);
     }
    //  TODO: make page root
     return redirect()->back()->with('success', 'Your page was successfully updated');
 }

 /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
 public function destroy($id)
 {
     //
     $page = Page::findOrFail($id);
     if (!$page->isRoot()) {
       $newParent = $page->parent()->get();
     }
     $pageLevel = $page->getLevel();
     $descendants = $page->getImmediateDescendants();
     foreach ($descendants as $node) {
       if (isset($newParent)) {
         $node->makeChildOf($newParent);
       } else {
         $node->makeRoot();
       }
     }
     $page->delete();
     return redirect()->back()->with('trash', ['You page has been moved to trash',  $id]);
 }

 public function forceDestroy($id)
 {
     //
     $page = Page::onlyTrashed()->findOrFail($id);
    //  $this->removeImage($page->image);
     $page->forceDelete();

     return redirect()->back()->with('danger', 'You page has been permanently deleted');
 }

 public function restore($id) {
   //restore trashed pages
   $page = Page::withTrashed()->findOrFail($id);
   $page->restore();
   return redirect()->back()->with('success', 'Your page has been successfully restored');
 }

 private function removeImage($image) {
   if (!empty($image)) {
     $imagePath = $this->uploadPath . "/" . $image;
     $ext = substr(strrchr($image, '.'), 1);
     $thumbnail = str_replace(".{$ext}", "_thumb.{$ext}", $image);
     $thumbPath = $this->uploadPath . "/" . $thumbnail;
     if (file_exists($imagePath)) {
       unlink($imagePath);
     }
     if (file_exists($thumbPath)) {
       unlink($thumbPath);
     }
   }
 }
}
