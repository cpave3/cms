<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(Request $request)
	{

		$rules = [
				  'name' => 'required',
				  'email' => 'required|email|unique:users',
				  'slug' => 'required|unique:users',
				  'role_id' => 'filled',
				  'password' => 'required|min:6',
			];

			switch($this->method()) {
				case 'PUT';
				case 'PATCH';
					$rules['slug'] = 'required|unique:users,slug,' . $this->route('user');
					$rules['name'] = 'required|unique:users,name,' . $this->route('user');
					break;
			}

			return $rules;
		}
	}
