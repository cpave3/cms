@extends('layouts.pages')
@section('title', 'Events')
@section('content')

    <div class="row"> <!-- START event banner -->
        <button class="past-events-link btn">
            <a href="{{url('/past-events')}}">
                PAST EVENTS
            </a>
        </button>

        <div class="container-fluid">
            <section class="event-banner" data-speed="6">
                <div class="row">
                    <div class="col-md-12">
                        <h1>LATEST EVENTS</h1>
                    </div>
                </div>
            </section>
        </div>
    </div><!-- END event banner -->

    @include('snippets.backend.events.event-thumbnails')

@endsection
