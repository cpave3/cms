<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use GrahamCampbell\Markdown\Facades\Markdown;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * App\User
 *
 * @property-read mixed $bio_html
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts() {
      return $this->hasMany('App\Post');
    }

    public function uploads() {
      return $this->hasMany('App\Upload');
    }

    public function getBioHtmlAttribute ($value) {
      return $this->bio ? Markdown::convertToHtml(e($this->bio)) : NULL;
    }

    public function gravatar() {
      $email = $this->email;
      // $default = asset("img/author.jpg");
      $default = "http://1plusx.com/app/mu-plugins/all-in-one-seo-pack-pro/images/default-user-image.png";
      $size = 100;

      return "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
    }

	public static function updateUser(UserRequest $request, User $user)
	{
		$slug = strtolower(str_replace(' ', '-', $request->name));
		$request->merge(['slug' => $slug]);
		$input = $request->all();
		$user = User::findOrFail($user->id);
		$updateNow = $user->update($input);
	}

	public static function changePassword(UserRequest $request, User $user)
	{
		$request->merge(['password' => Hash::make($request->password)]);
		$input = $request->all();
		$user = User::findOrFail($user->id);
		$updateNow = $user->update($input);
	}

	public function role()
	{
		//return $this->belongsTo('App\Role');
		return $this->belongsTo(Role::class, 'role_id');
	}

	//checks the role of the user logged in
	public static function checkUserRole()
	{
		if (Auth::user()->role->name == 'Admin') {
			return "Admin";
		}
		else if (Auth::user()->role->name == 'Author') {
			return "Author";
		}
		else if (Auth::user()->role->name == 'Contributor') {
			return "Contributor";
		}
		else if (Auth::user()->role->name == 'Super Admin') {
			return "Super Admin";
		}
	}

}
