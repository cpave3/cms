<div class="{{ 'confirmationModal' . $user->id }} modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete {{ $user->name }}</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete {{ $user->name }}?</p>
            </div>
            <div class="modal-footer">
                <form method="POST" action="{{ url('admin/users/' . $user->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <div class="form-group">
                        <button type="submit" class="btn bg-green">Yes</button>
                        <button class="btn bg-red" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>