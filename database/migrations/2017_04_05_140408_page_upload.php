<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageUpload extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up() {
       Schema::create('page_upload', function(Blueprint $table) {
         $table->increments('id');
         $table->integer('page_id');
         $table->integer('upload_id');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down() {
       Schema::drop('page_upload');
     }
}
