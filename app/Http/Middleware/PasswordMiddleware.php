<?php

namespace App\Http\Middleware;

use App\User;
use Auth;
use Closure;
use Route;

class PasswordMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$routeParameter = Route::getCurrentRoute()->parameters;
		$user = User::findOrFail($routeParameter);

    	//dd(Route::getCurrentRoute());
		// Super Admin has full power to edit anyone's password. If the user is not a Super Admin or Admin, they can only change their own password
		if ($request->user()->role->name == 'Super Admin' || $routeParameter["user"] == Auth::id())
		{
			return $next($request);
		}
		//Admin's can only change a user's password if they are not a Super Admin or other Admins
		else if ($request->user()->role->name == 'Admin' && ($user[0]->role->name != 'Super Admin' && $user[0]->role->name != 'Admin')) {
			return $next($request);
		}
		else {
			return redirect('/admin');
		}

    }
}
