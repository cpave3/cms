<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('events', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 191);
			$table->string('slug');
			$table->dateTime('event_date');
			$table->dateTime('expiry_date');
			$table->string('location', 191);
			$table->string('excerpt', 255);
			$table->text('body');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('events');
    }
}
