@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Mailing List
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.lists.index')}}"><i class="fa fa-list"></i> Mailing Lists</a></li>
        <li class="active"><i class="fa fa-plus"></i> &nbsp;Create New List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {!! Form::model($subscription, [
            'method' => 'POST',
            'route' => 'admin.lists.store'
            ]) !!}
          @include('snippets.backend.lists.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  {{-- @include('snippets.backend.scripts.lists') --}}
@endsection
