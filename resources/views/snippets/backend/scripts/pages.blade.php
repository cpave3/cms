<script type="text/javascript">

$("#draft-btn").click(function(e) {
  e.preventDefault();
  $("#published_at").val("")
  $("#pages-form").submit();
});

$("#submit-btn").click(function(e) {
  e.preventDefault();
  dateNow = new Date().toISOString().slice(0, 19).replace('T', ' ');
  $("#published_at").val(dateNow);
  $("#pages-form").submit();
});

$('ul.pagination').addClass('no-margin pagination-sm');

$('#title').on('blur', function() {
  var theTitle = this.value.toLowerCase().trim(),
      slugInput = $('#slug'),
      theSlug = theTitle.replace(/&/g, '-and-').replace(/[^a-z0-9-]+/g, '-').replace(/\-\-+/g, '-').replace(/^-+|-+$/g);
  if (slugInput.val() == "") {
    slugInput.val(theSlug);
  }
});

$("#slug").on("keyup", function() {
  this.value = this.value
  .toLowerCase()
  .trim()
  .replace(/&/g, '-and-')
  // .replace(/[^\w\s&-]/g, '')
  .replace(/[^a-z0-9-]+/g, '-')
  .replace(/\-\-+/g, '-')
  .replace(/^-+|-+$/g, '-');
});

//Not needed?
// $(document).ready(function () {
//     $('.repeater').repeater()
// });

 function refreshAttachments() {
   $.ajax({
    method: "GET",
    url: "{{route('admin.uploads.ajax.attachments', $page->id)}}",
  })
    .done(function( response ) {

      $("#attachments").html(response);
      $('.ajax-delete-form').on('submit', function() {
        return confirm('Are you sure you wish to permanatley delete this attachment?');
      });
      $('.ajax-delete-form').ajaxForm({
        success: function(res) {
            console.log(res);
            refreshAttachments();
        }
      });

      $('.ajax-hide-form').ajaxForm({
        success: function(res) {
            console.log(res);
            refreshAttachments();
        }
      });
    });
 }

function disableUploadButton(id) {
  $button = $("#"+id);
  $button.prop('disabled', true);
  $button.toggleClass("btn-success");
  $button.children("span").text("Uploading...  ");
  $button.children("i").toggleClass("off")
  $button.children("i").toggleClass("spinner");
}

function enableUploadButton(id) {
  $button = $("#"+id);
  $button.prop('disabled', false);
  $button.toggleClass("btn-success");
  $button.children("span").text("Upload Attachment");
  $button.children("i").toggleClass("off")
  $button.children("i").toggleClass("spinner");
}

$(function() {
  var optionsStore = {
  url:     '{{route("admin.uploads.store")}}',
  type:     'POST',
  success: function(response) {
    resetForm('attachment-form');
    console.log("working");
    refreshAttachments();
    enableUploadButton("attachment-submit");

  }
};

//allow submissions of attachments via ajax
$('#attachment-form').ajaxForm(optionsStore);
});

$('.ajax-delete-form').on('submit', function() {
  return confirm('Are you sure you wish to permanatley delete this attachment?');
});
// $(function() {
// pass options to ajaxForm
$('.ajax-delete-form').ajaxForm({
  success: function(res) {
      console.log(res);
      refreshAttachments();
  }
});

$('.ajax-hide-form').ajaxForm({
  success: function(res) {
      console.log(res);
      refreshAttachments();
  }
});


$('#attachment-submit').on('click', function() {
  disableUploadButton("attachment-submit");
  $("#attachment-form").submit();
});


</script>
