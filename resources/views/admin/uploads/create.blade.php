@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Upload
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.blog.index')}}"><i class="fa fa-upload"></i> Uploads</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;New Upload</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-sm-12">
            @include('snippets.backend.message')
          </div>
          {!! Form::model($post, [
            'method' => 'POST',
            'route' => 'admin.uploads.store',
            'files' => TRUE,
            'id' => 'upload-form'
            ]) !!}
          @include('snippets.backend.uploads.form')
          {!! Form::close() !!}
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.blog')
@endsection
