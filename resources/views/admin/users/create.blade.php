@extends('layouts.backend')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add User
            </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-user"></i> Users</li>
                <li class="active"><i class="fa fa-pencil"></i> Add User</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body ">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Register</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.users.store') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>

                                                            @if ($errors->has('name'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                                        <label for="slug" class="col-md-4 control-label">Slug</label>

                                                        <div class="col-md-6">
                                                            <input id="slug" type="text" class="form-control" name="slug" value="{{ old('slug') }}" autofocus>

                                                            @if ($errors->has('slug'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('slug') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                                                        <label for="role_id" class="col-md-4 control-label">Role</label>

                                                        <div class="col-md-6">
                                                            <select id="role_id" class="form-control" name="role_id">
                                                                <option selected="selected" value="">Please select a role</option>
                                                                @foreach($roles as $role)
                                                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                                @endforeach
                                                            </select>

                                                            @if ($errors->has('role_id'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('role_id') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label for="password" class="col-md-4 control-label">Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control" name="password">

                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <a class="btn bg-gray-light" href="{{ url('admin/users') }}">Cancel</a>
                                                            <button type="submit" class="btn btn-primary">
                                                                Register
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')
  <script type="text/javascript">
  $('#name').on('blur', function() {
    var theTitle = this.value.toLowerCase().trim(),
        slugInput = $('#slug'),
        theSlug = theTitle.replace(/&/g, '-and-').replace(/[^a-z0-9-]+/g, '-').replace(/\-\-+/g, '-').replace(/^-+|-+$/g);
    if (slugInput.val() == "") {
      slugInput.val(theSlug);
    }
  });

  $("#slug").on("keyup", function() {
    this.value = this.value
    .toLowerCase()
    .trim()
    .replace(/&/g, '-and-')
    // .replace(/[^\w\s&-]/g, '')
    .replace(/[^a-z0-9-]+/g, '-')
    .replace(/\-\-+/g, '-')
    .replace(/^-+|-+$/g, '-');
  });
  </script>
@endsection
