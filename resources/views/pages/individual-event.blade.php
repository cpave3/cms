@extends('layouts.pages')
@section('title', $event->title)
@section('content')

    <div class="row"> <!-- START EVENT DETAIL banner -->
        <div class="container-fluid">
            <section class="event-banner" data-speed="6"></section>
        </div>
    </div><!-- END EVENT DETAIL banner -->

    <div class="row"> <!-- START events -->
        <div class="col-md-10 col-md-offset-1">
            <div class="event-details-container container">
                <article>
                    <div class="row"> <!-- START events -->
                        <div class="event-image col-md-8">
                            <img src="../img/header-bg.jpg" alt="event image">
                        </div>

                        <div class="col-md-4">
                            <section class="event-information">
                                <h1>{{ $event->title }}</h1>
                                <h4>When: {{ $event::formatdate($event->event_date) }}</h4>
                                <h4>Where: {{ $event->location }}</h4>
                            </section>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div><!--END events -->

    <div class="row"> <!-- START events detail -->
        <div class="col-md-10 col-md-offset-1">
            <div class="event-details-content-container container">
                <h2 class="event-excerpt">{{ $event->excerpt }}</h2>
                <article class="event-details">
                    <p><pre class="content"> {{ $event->body }} </pre></p>
                </article>
            </div>
        </div>
    </div><!--END events detail -->

@endsection
