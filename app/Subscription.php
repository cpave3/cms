<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    //
    use softDeletes;

    protected $fillable = [
        'name',
        'description',
        'from_name',
        'from_email'
    ];

    public function subscribers() {
  		return $this->belongsToMany('App\Subscriber', 'subscriber_subscription');
  	}

    public function mailouts() {
      return $this->belongsToMany('App\Mail', 'mail_subscription');
    }
}
