@extends('layouts.pages')
@section('title', 'Queensland Advocacy Incorporated')
@section('content')
  <section id="content" style="min-height: 56vh;">
    <div class="container" style="text-align: center;">
      <div class="row">
        <div class="col-sm-12">
          <h1>404, Resource not found</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          The page you were looking for could not be found.
        </div>
      </div>
    </div>
  </section>
@endsection
