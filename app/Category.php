<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Category
 *
 * @property-read mixed $count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Post[] $posts
 * @mixin \Eloquent
 */
class Category extends Model
{
    //
    protected $fillable = [
      'title',
      'slug'
    ];
    public function posts() {
      return $this->hasMany('App\Post');
    }

    public function getCountAttribute($value) {
      return count($this->posts);
    }
}
