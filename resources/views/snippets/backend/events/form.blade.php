<div class="col-sm-8 col-md-9">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body ">

            <div class="form-group {{$errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title')!!}
                {!! Form::text('title', null, ['class' => 'form-control'])!!}
                @if ($errors->has('title'))
                    <span class="help-block">{{$errors->first('title')}}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('location') ? 'has-error' : ''}}">
                {!! Form::label('location')!!}
                {!! Form::text('location', null, ['class' => 'form-control'])!!}
                @if ($errors->has('location'))
                    <span class="help-block">{{$errors->first('location')}}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('event_date') ? 'has-error' : ''}}">
                {!! Form::label('event_date', 'Event Date')!!}
                {!! Form::text('event_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'When is the event on?'])!!}
                @if ($errors->has('event_date'))
                    <span class="help-block">{{$errors->first('event_date')}}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('excerpt') ? 'has-error' : ''}}">
                {!! Form::label('excerpt')!!}
                {!! Form::textarea('excerpt', null, ['class' => 'form-control excerpt', 'rows' => '3'])!!}
                @if ($errors->has('excerpt'))
                    <span class="help-block">{{$errors->first('excerpt')}}</span>
                @endif
            </div>

            <div class="form-group {{$errors->has('body') ? 'has-error' : ''}}">
                {!! Form::label('body')!!}
                {!! Form::textarea('body', null, ['class' => 'form-control tinymce'])!!}
                @if ($errors->has('body'))
                    <span class="help-block">{{$errors->first('body')}}</span>
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<div class="col-sm-4 col-md-3">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">
                Publish
            </h3>
        </div>
        <div class="box-body clearfix">
            <div class="pull-left">
                <a class="btn bg-gray-light" href="{{ url('admin/events') }}">Cancel</a>
            </div>
            <div class="pull-right">
                {!! Form::submit('Publish Event', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
    </div>
</div>
