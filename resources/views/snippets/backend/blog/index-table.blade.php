<table class="table table-bordered" id="data-table">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="20">ID</td>
      <td>Title</td>
      <td width="120">Author</td>
      <td width="150">Category</td>
      <td width="120">Date</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($posts as $post)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.blog.destroy', $post->id]]) !!}
        <a href="{{route('admin.blog.edit', $post->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$post->id}}</td>
        <td>{{$post->title}}</td>
        <td>{{$post->author->name}}</td>
        <td>{{$post->category->title}}</td>
        <td>
          <abbr title="{{$post->dateFormatted(true)}}">{{$post->dateFormatted()}}</abbr> &nbsp;
          {!! $post->publicationLabel !!}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
