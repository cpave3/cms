<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//reset table
		DB::table('roles')->truncate();

		//Generate new data
		DB::table('roles')->insert([
			  		[
			  			  'name' => 'Super Admin',
					],
					[
						  'name' => 'Admin',
					],
					[
						  'name' => 'Author',
					],
					[
						  'name' => 'Contributor',
					]
			  ]
		);
    }
}
