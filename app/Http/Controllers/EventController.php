<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Event;
use App\User;

class EventController extends Controller
{
	public function show($path) {
		/*
		TODO: Refactor
		IDEA: See Below

		Not sure of a better way to do this, grabbing all pages and then checking the path
		for each to see if there is a match. Would rather do this with SQL, but the path
		is dynamically generated from the slugs of a page tree, not stored in DB

		Look into Baum's SQL guide to see if we can dynamically get the path the same way, but in SQL instead of PHP
		This would make this query more efficient. not an issue now, but could be with lots of pages.

		*/
		$pages = Page::published()->get();
		foreach ($pages as $p) {
			if ($p->path == $path) {
				$page = $p;
			}
		}

		if (isset($page) && !empty($page)) {
			//return view here
			return view('pages.internal', compact('page'));
			// return $page->title;
		} else {
			// handle 404 here
			abort(404, 'No Page Found');
		}
	}

	public function home() {
		$upcomingEvents = Event::getFirstThreeUpcomingEvents();
		$noEvents = Event::checkForUpcomingEvents($upcomingEvents);
		$subscribed = false;
		return view('pages.home', compact('subscribed', 'upcomingEvents', 'noEvents'));
	}

	public function events() {
		$events = Event::getCurrentEvents();
		$subscribed = false;
		$eventsAMonthAgo = Event::getEventsAMonthAgo();

		if (count($events) == 0) {
			$noEventsMsg = "Sorry. There are no upcoming events";
			return view('events.current-events', compact('events', 'subscribed', 'noEventsMsg', 'eventsAMonthAgo'));
		} else {
			return view('events.current-events', compact('events', 'subscribed', 'eventsAMonthAgo'));
		}
	}

	public function eventDetails(Event $event) {
		$event = Event::find($event);
		//$text = $event::preserveTextFormat($event->body);
		$subscribed = false;
		//$user = User::where('slug', $user)->first();
		return view('events.event-details', compact('event', 'subscribed'));
	}

	public function pastEvents() {
		$events = Event::getPastEvents();
		$subscribed = false;
		if (count($events) == 0) {
			$noEventsMsg = "No past events";
			return view('events.past-events', compact('events', 'subscribed', 'noEventsMsg'));
		} else {
			return view('events.past-events', compact('events', 'subscribed'));
		}
	}
}
