<script type="text/javascript">
    @foreach(Auth::user()->all() as $user)

        @if(Auth::id() !== $user->id)
            $("{{'#deleteUser' . $user->id}}").click(function(){
                $("{{'.confirmationModal' . $user->id}}").modal({
                    backdrop: "static"
                });
            });
        @endif

    @endforeach
</script>