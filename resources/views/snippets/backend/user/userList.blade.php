<table class="table table-bordered" id="data-table">
    <thead>
    <tr>
        <th> Action </th>
        <th> User Id </th>
        <th> Name </th>
        <th> Email </th>
        <th> Role </th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach(Auth::user()->all() as $user)
        <tr class="table-row">
            <td>
                @if( auth()->id() == $user->id || auth()->user()->role->name == 'Super Admin')
                    <a class="btn btn-xs btn-default" href="{{ route('admin.users.edit', $user->id) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                @elseif ( auth()->user()->role->name == 'Admin' && ($user->role->name != 'Super Admin' && $user->role->name != 'Admin') )
                    <a class="btn btn-xs btn-default" href="{{ route('admin.users.edit', $user->id) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                @else

                @endif

                @if( auth()->id() != $user->id && auth()->user()->role->name == 'Super Admin')
                    <button id="{{'deleteUser' . $user->id}}" class="btn btn-xs btn-danger" data-toggle="modal" data-target="{{'#confirmationModal' . $user->id}}">
                        <i class="fa fa-trash"></i>
                    </button>
                    @include('snippets.backend.modal.confirmationModal')
                @elseif ( auth()->user()->role->name == 'Admin' && ($user->role->name != 'Super Admin' && $user->role->name != 'Admin') )
                    <button id="{{'deleteUser' . $user->id}}" class="btn btn-xs btn-danger" data-toggle="modal" data-target="{{'#confirmationModal' . $user->id}}">
                        <i class="fa fa-trash"></i>
                    </button>
                    @include('snippets.backend.modal.confirmationModal')
                @else

                @endif
            </td>

            <td> {{ $user->id }} </td>
            <td> {{ $user->name }} </td>
            <td> {{ $user->email }} </td>
            <td> {{ $user->role->name }} </td>


            @if(auth()->id() == $user->id || ($user->role->name != 'Super Admin' && $user->role->name != 'Admin'))
                <td>
                    <a class="btn btn-pinterest" href="{{ url('admin/users/' . $user->id .'/change-password') }}">Change Password</a>
                </td>
            @else
                <td></td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
