<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    //
    protected $fillable = [
      'name',
      'body',
      'from_email',
      'from_name',
      'subject',
      'description'
    ];
    protected $dates = [
      'sent_at'
    ];
    public function subscriptions() {
      return $this->belongsToMany('App\Subscription', 'mail_subscription');
    }
}
