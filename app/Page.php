<?php
namespace App;
use Baum\Node;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use GrahamCampbell\Markdown\Facades\Markdown;
/**
* Page
*/
class Page extends Node {
  use SoftDeletes;
  protected $dates = ['published_at'];

  /**
   * Table name.
   *
   * @var string
   */
  protected $table = 'pages';

  protected $fillable = [
    'title',
    'excerpt',
    'body',
    'slug',
    'published_at',
    'menu_id'
  ];


  protected $parentColumn = 'parent_id';

  // /**
  //  * Column name for the left index.
  //  *
  //  * @var string
  //  */
  protected $leftColumn = 'lft';

  // /**
  //  * Column name for the right index.
  //  *
  //  * @var string
  //  */
  protected $rightColumn = 'rgt';

  // /**
  //  * Column name for the depth field.
  //  *
  //  * @var string
  //  */
  protected $depthColumn = 'depth';

  // /**
  //  * Column to perform the default sorting
  //  *
  //  * @var string
  //  */
  protected $orderColumn = null;

  // /**
  // * With Baum, all NestedSet-related fields are guarded from mass-assignment
  // * by default.
  // *
  // * @var array
  // */
  protected $guarded = array('id', 'parent_id', 'lft', 'rgt', 'depth');

  public function getIndentedAttribute() {
    // return str_repeat(html_entity_decode('&emsp;', 1), $this->depth) . $this->title;
    return $this->title;
  }

  public function attachments() {
    return $this->belongsToMany('App\Upload', 'page_upload');
  }

  public function menu() {
    return $this->belongsTo('App\Menu');
  }


  public function getPathAttribute()  {

    if ($this->isRoot()) {
      return $this->slug;
    } else {

    $ancestors = [];
    foreach ($this->getAncestorsAndSelf() as $node) {
      $ancestors[] = $node->slug;
    }
    if (!empty($ancestors)) {
      return implode('/', $ancestors);
    } else {
      return die('Something went wrong');
    }
  }
}

public function setPublishedAtAttribute($value) {
  $this->attributes['published_at'] = $value ?: NULL;
}

public function getDateAttribute($value) {
  return is_null($this->published_at) ? '' : $this->published_at->diffForHumans();
}

public function getBodyHtmlAttribute ($value) {
  return $this->body ? Markdown::convertToHtml(e($this->body)) : NULL;
}

public function getExcerptHtmlAttribute ($value) {
  return $this->excerpt ? Markdown::convertToHtml(e($this->excerpt)) : NULL;
}

public function dateFormatted($showTimes = false) {
  $format = 'd/m/Y';
  if ($showTimes) {$format = $format . " H:i:s";}
  return $this->created_at->format($format);
}

public function getPublicationLabelAttribute() {
  if ($this->deleted_at) {
      return "<span class='label label-danger'>Trashed</span>";
  } elseif (!$this->published_at) {
      return "<span class='label label-warning'>Draft</span>";
  } elseif ($this->published_at && $this->published_at->isFuture() ) {
      return "<span class='label label-info'>Scheduled</span>";
  } else {
      return "<span class='label label-success'>Published</span>";
  }
}

//SCOPES
public function scopeLatestFirst($query) {
  return $query->orderBy('created_at', 'desc');
}

public function scopePublished($query) {
  return $query->where('published_at', '<=', Carbon::now());
}

public function scopeDraft($query) {
  return $query->where('published_at', '=', null);
}


}
