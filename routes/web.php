<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Home Route - default home page
Route::get('/', 'PageController@home');

Route::get('/events', 'EventController@events');

Route::get('/past-events', 'EventController@pastEvents');

Route::get('/events/{event}', [
	  'uses' => 'EventController@eventDetails',
	  'as' => 'event.index'
]);

Route::post('search', ['as' => 'search', 'uses' => 'SearchController@search']);
//Route::get('search', ['as' => 'search', 'uses' => 'SearchController@show']);

//refactor and use Ajax
Route::post('/', [
	  'uses' => 'SubscribeController@store',
	  'as' => 'subscriber.store'
]);

//BLOG ROUTES
Route::get('/blog', [
  'uses' => 'BlogController@index',
  'as' => 'blog.index'
]);

Route::get('/blog/{post}', [
  'uses' => 'BlogController@single',
  'as' => 'blog.single'
]);

Route::get('/blog/category/{category}', [
  'uses' => 'BlogController@category',
  'as' => 'blog.category'
]);

Route::get('/blog/author/{author}', [
  'uses' => 'BlogController@author',
  'as' => 'blog.author'
]);

Auth::routes();

// Dashboard Routes
Route::get('/admin', [
	  'uses' => 'Backend\DashboardController@index',
	  'as' => 'admin.dashboard'
]);

Route::group(['middleware' => 'App\Http\Middleware\PasswordMiddleware'], function()
{
// Route::group(['middleware' => 'App\Http\Middleware\PasswordMiddleware'], function()
// {
	Route::patch('/admin/users/{user}/change-password', [
		'uses' => 'Backend\AdminUserController@updatePassword',
		'as' => 'admin.users.updatePassword'
	]);
//Lists
Route::resource('/admin/lists', 'Backend\ListController', [
	  'names' => [
			'store' => 'admin.lists.store',
			'index' => 'admin.lists.index',
			'create' => 'admin.lists.create',
			'update' => 'admin.lists.update',
			'destroy' => 'admin.lists.destroy',
			'edit' => 'admin.lists.edit',
	  ]
]);

//Users
Route::resource('/admin/users', 'Backend\AdminUserController', [
	  'names' => [
			'store' => 'admin.users.store',
			'index' => 'admin.users.index',
			'create' => 'admin.users.create',
			'update' => 'admin.users.update',
			'destroy' => 'admin.users.destroy',
			'edit' => 'admin.users.edit',
	  ]
]);

	Route::get('/admin/users/{user}/change-password', 'Backend\AdminUserController@changePassword');
}); //END PASSWORD MIDDLEWARE

Route::patch('/admin/users/{user}/change-password', [
	  'uses' => 'Backend\AdminUserController@updatePassword',
	  'as' => 'admin.users.updatePassword'
]);

Route::group(['middleware' => 'App\Http\Middleware\AdminMiddleware'], function()
{
	//Can only access these routes if the logged in user as an Admin or Super Admin

	//Users
	Route::resource('/admin/users', 'Backend\AdminUserController', [
		  'names' => [
				'store' => 'admin.users.store',
				'index' => 'admin.users.index',
				'create' => 'admin.users.create',
				'show' => 'admin.users.show',
				'update' => 'admin.users.update',
				'destroy' => 'admin.users.destroy',
				'edit' => 'admin.users.edit',
		  ]
	]);

	// Blog
	Route::resource('/admin/posts', 'Backend\AdminBlogController', [
		  'names' => [
				'store' => 'admin.blog.store',
				'index' => 'admin.blog.index',
				'create' => 'admin.blog.create',
				'show' => 'admin.blog.show',
				'update' => 'admin.blog.update',
				'destroy' => 'admin.blog.destroy',
				'edit' => 'admin.blog.edit',
		  ]
	]);
	Route::put('admin/posts/{post}/restore', [
		  'uses' => 'Backend\AdminBlogController@restore',
		  'as' => 'admin.blog.restore'
	]);
	Route::delete('admin/posts/{post}/force', [
		  'uses' => 'Backend\AdminBlogController@forceDestroy',
		  'as' => 'admin.blog.force-destroy'
	]);

	//categories
	Route::resource('/admin/categories', 'Backend\AdminCategoryController', [
		  'names' => [
				'store' => 'admin.categories.store',
				'index' => 'admin.categories.index',
				'create' => 'admin.categories.create',
				'show' => 'admin.categories.show',
				'update' => 'admin.categories.update',
				'destroy' => 'admin.categories.destroy',
				'edit' => 'admin.categories.edit',
		  ]
	]);

	// Pages
	Route::resource('/admin/pages', 'Backend\AdminPageController', [
		  'names' => [
				'store' => 'admin.pages.store',
				'index' => 'admin.pages.index',
				'create' => 'admin.pages.create',
				'show' => 'admin.pages.show',
				'update' => 'admin.pages.update',
				'destroy' => 'admin.pages.destroy',
				'edit' => 'admin.pages.edit',
		  ]
	]);
	Route::put('admin/pages/{page}/restore', [
		  'uses' => 'Backend\AdminPageController@restore',
		  'as' => 'admin.pages.restore'
	]);
	Route::delete('admin/pages/{page}/force', [
		  'uses' => 'Backend\AdminPageController@forceDestroy',
		  'as' => 'admin.pages.force-destroy'
	]);

	// Events
	Route::resource('/admin/events', 'Backend\AdminEventController', [
		  'names' => [
				'store' => 'admin.events.store',
				'index' => 'admin.events.index',
				'create' => 'admin.events.create',
				'show' => 'admin.events.show',
				'update' => 'admin.events.update',
				'destroy' => 'admin.events.destroy',
				'edit' => 'admin.events.edit',
		  ]]);


	// Uploads
	Route::resource('/admin/uploads', 'UploadController', [
			'names' => [
				'store' => 'admin.uploads.store',
				'index' => 'admin.uploads.index',
				'create' => 'admin.uploads.create',
				'update' => 'admin.uploads.update',
				'destroy' => 'admin.uploads.destroy',
				'edit' => 'admin.uploads.edit',
			]]);
	Route::get('/admin/uploads/ajax/pages/{id}', [
		'as' => 'admin.uploads.ajax.attachments',
		'uses' => 'UploadController@ajaxPages'
	]);
	Route::get('/admin/uploads/ajax/posts/{id}', [
		'as' => 'admin.uploads.ajax.attachments2',
		'uses' => 'UploadController@ajaxPosts'
	]);
	Route::put('/admin/uploads/hide/{id}', [
		'as' => 'admin.uploads.hideToggle',
		'uses' => 'UploadController@hideToggle'
	]);


	// Mailing Lists
	Route::resource('/admin/lists', 'Backend\ListController', [
		  'names' => [
				'store' => 'admin.lists.store',
				'index' => 'admin.lists.index',
				'create' => 'admin.lists.create',
				'show' => 'admin.lists.show',
				'update' => 'admin.lists.update',
				'destroy' => 'admin.lists.destroy',
				'edit' => 'admin.lists.edit',
		  ]
	]);
	Route::put('admin/lists/{list}/restore', [
		  'uses' => 'Backend\ListController@restore',
		  'as' => 'admin.lists.restore'
	]);
	Route::delete('admin/lists/{list}/force', [
		  'uses' => 'Backend\ListController@forceDestroy',
		  'as' => 'admin.lists.force-destroy'
	]);

	// Mailing Lists
	Route::resource('/admin/mail', 'Backend\MailController', [
		  'names' => [
				'store' => 'admin.mail.store',
				'index' => 'admin.mail.index',
				'create' => 'admin.mail.create',
				'show' => 'admin.mail.show',
				'update' => 'admin.mail.update',
				'destroy' => 'admin.mail.destroy',
				'edit' => 'admin.mail.edit',
		  ]
	]);
	Route::post('admin/mail/send/{id}', [
		'uses' => 'Backend\MailController@send',
		'as' => 'admin.mail.send'
		]);
	Route::post('/admin/menus/data', [
		'uses' => 'Backend\MenuController@data',
		'as' => 'admin.menus.data'
		]);
	Route::resource('/admin/menus', 'Backend\MenuController', [
	  'names' => [
			'store' => 'admin.menus.store',
			'index' => 'admin.menus.index',
			'create' => 'admin.menus.create',
			'update' => 'admin.menus.update',
			'destroy' => 'admin.menus.destroy',
			'edit' => 'admin.menus.edit',
	  ]
	]);
	Route::get('/admin/home/edit', [
		'uses' => 'Backend\HomeController@edit',
		'as' => 'admin.home.edit'
		]);
	Route::put('/admin/home/store/{id}', [
		'uses' => 'Backend\HomeController@update',
		'as' => 'admin.home.update'
		]);

}); //END ADMIN MIDDLEWARE

//Route::delete('/admin/users/{user}', 'Backend\AdminUserController@destroy');




// Default Catch-all for pages
Route::get('/{path}', 'PageController@show')->where('path', '(.*)')->name("pages.show");
