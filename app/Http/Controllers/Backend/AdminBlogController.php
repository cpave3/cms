<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Http\Requests\PostRequest;
use Intervention\Image\Facades\Image;

class AdminBlogController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $uploadPath;

     public function __construct() {
       parent::__construct();
       $this->uploadPath = public_path(config('cms.image.directory'));
     }

    public function index(Request $request)
    {
        //
        $onlyTrashed = FALSE;
        if (($status = $request->get('status')) && $status == 'trash')
        {
          $posts = Post::onlyTrashed()->with('category', 'author')->latest()->get();
          $postCount = Post::onlyTrashed()->count();
          $onlyTrashed = TRUE;
        }
        elseif (($status = $request->get('status')) && $status == 'published')
        {
          $posts = Post::published()->with('category', 'author')->latest()->get();
          $postCount = Post::published()->count();
        }
        elseif (($status = $request->get('status')) && $status == 'scheduled')
        {
          $posts = Post::scheduled()->with('category', 'author')->latest()->get();
          $postCount = Post::scheduled()->count();
        }
        elseif (($status = $request->get('status')) && $status == 'draft')
        {
          $posts = Post::draft()->with('category', 'author')->latest()->get();
          $postCount = Post::draft()->count();
        }
        else
        {
          $posts = Post::with('category', 'author')->latest()->get();
          $postCount = Post::count();
        }

        $statusList = $this->statusList();
        return view('admin.posts.index', compact('posts', 'postCount', 'onlyTrashed', 'statusList'));
    }

    private function statusList() {
      return [
        'all' => Post::count(),
        'published' => Post::published()->count(),
        'scheduled' => Post::scheduled()->count(),
        'draft' => Post::draft()->count(),
        'trash' => Post::onlyTrashed()->count(),
      ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        //
        return view('admin.posts.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //
        // $this->validate($request, [
        //
        // ]);

        $data = $this->handleRequest($request);
        $post = $request->user()->posts()->create($data);

        return redirect(route('admin.blog.edit', $post->id))->with('message', 'Your post was successfully created');
    }

    private function handleRequest($request) {
      $data = $request->all();
      if ($request->hasFile('image')) {
        $image = $request->file('image');
        $fileName = $image->getClientOriginalName();

        $destination = $this->uploadPath;
        $successUploaded = $image->move($destination, $fileName);

        if ($successUploaded) {
          $extention = $image->getClientOriginalExtension();
          $thumbnail = str_replace(".{$extention}", "_thumb.{$extention}", $fileName);

          //scale to 250px wide
          $width = config('cms.image.thumbnail.width');
          $height = config('cms.image.thumbnail.height');
          Image::make($destination . '/' . $fileName)
          ->resize($width,$height)
          ->save($destination . '/' . $thumbnail);

          //crop to 250px x 175px
          // Image::make($destination . '/' . $fileName)
          // ->crop($width, $height)
          // ->save($destination . '/' . $thumbnail);
        }

        $data['image'] = $fileName;
      }
      return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        //
        $post = Post::findOrFail($id);
        $data = $this->handleRequest($request);
        $oldImage = $post->image;
        $post->update($data);
        if ($oldImage !== $post->image) {
          $this->removeImage($oldImage);
        }
        return redirect()->back()->with('success', 'Your post was successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Post::findOrFail($id)->delete();
        return redirect()->back()->with('trash', ['You blog post has been moved to trash',  $id]);
    }

    public function forceDestroy($id)
    {
        //
        $post = Post::onlyTrashed()->findOrFail($id);
        $this->removeImage($post->image);
        $post->forceDelete();

        return redirect()->back()->with('danger', 'You blog post has been permanently deleted');
    }

    public function restore($id) {
      //restore trashed posts
      $post = Post::withTrashed()->findOrFail($id);
      $post->restore();
      return redirect()->back()->with('success', 'Your post has been successfully restored');
    }

    private function removeImage($image) {
      if (!empty($image)) {
        $imagePath = $this->uploadPath . "/" . $image;
        $ext = substr(strrchr($image, '.'), 1);
        $thumbnail = str_replace(".{$ext}", "_thumb.{$ext}", $image);
        $thumbPath = $this->uploadPath . "/" . $thumbnail;
        if (file_exists($imagePath)) {
          unlink($imagePath);
        }
        if (file_exists($thumbPath)) {
          unlink($thumbPath);
        }
      }
    }
}
