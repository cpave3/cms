@extends('layouts.backend')
@section('title', 'Users')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
            </h1>
            <ol class="breadcrumb">
                <li class="active"><i class="fa fa-user"></i> Users</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @if(isset($cannotDeleteUser) && $cannotDeleteUser == true)
                        <div class="box-body">
                        <p class="alert alert-danger"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> The user {{ $userName }} cannot be deleted</p>
                        </div>
                    @endif
                    <div class="box">
                        <div class="box-header clearfix">
                            <div class="pull-left">
                                <a href="{{route('admin.users.create')}}" class="btn btn-success">Add User &nbsp;<i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body ">
{{--                            @if($role == 'Subscriber')
                                <p>You are a subscriber</p>
                            @endif--}}

                            @include('snippets.backend.user.userList')

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('script')

<script type="text/javascript">

    $("#deleteUser").click(function(){
        $("#confirmationModal").modal({
            backdrop: "static",
            keyboard: false
        });
    });

</script>

    @include('snippets.backend.scripts.confirmationModal')
@endsection
