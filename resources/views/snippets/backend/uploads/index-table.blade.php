<table class="table table-bordered" id="data-table">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="20">ID</td>
      <td>Title</td>
      <td width="120">Author</td>
      <td width="150">Preview</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($uploads as $upload)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.blog.destroy', $upload->id]]) !!}
        <a href="{{route('admin.blog.edit', $upload->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$upload->id}}</td>
        <td>{{$upload->name}}</td>
        <td>{{$upload->user->name}}</td>
        <td><img height="100" src="{{url('/uploads/'.$upload->fileName)}}" alt=""></td>
      </tr>
    @endforeach
  </tbody>
</table>
