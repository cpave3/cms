<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;

class ListController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $onlyTrashed = FALSE;
        if (($status = $request->get('status')) && $status == 'trash')
        {
          $subscriptions = Subscription::onlyTrashed()->latest()->paginate(10);
          $onlyTrashed = TRUE;
        }
        else
        {
          $subscriptions = Subscription::paginate(10);
        }

        $statusList = $this->statusList();

        // $subscriptions = Subscription::all();
        $subscriptionCount = $subscriptions->count();
        return view('admin.mail.lists.index', compact('subscriptions', 'subscriptionCount', 'statusList', 'onlyTrashed'));
    }

    private function statusList() {
      return [
        'all' => Subscription::count(),
        'trash' => Subscription::onlyTrashed()->count(),
      ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subscription $subscription)
    {
        //
        return view('admin.mail.lists.create', compact('subscription'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $list = Subscription::create($data);

        return redirect(route('admin.lists.index'))->with('message', 'New list successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $subscription = Subscription::findOrFail($id);
      return view('admin.mail.lists.edit', compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $subscription = Subscription::findOrFail($id);
        $data = $request->all();
        $subscription->update($data);
        return redirect(route('admin.lists.index'))->with('message', 'List successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Subscription::findOrFail($id)->delete();
        return redirect(route('admin.lists.index'))->with('message', 'Mailing list successfully trashed');
    }

    public function restore($id) {
      // restore
      Subscription::withTrashed()->findOrFail($id)->restore();
      return redirect(route('admin.lists.index'))->with('message', 'Mailing list successfully restored');
    }

    public function forceDestroy($id) {
      //force delete
      Subscription::withTrashed()->findOrFail($id)->forceDelete();
      return redirect(route('admin.lists.index'))->with('message', 'Mailing list successfully Deleted');
    }
}
