@extends('layouts.backend')
@section('title', 'Create Event')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create New Event
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{route('admin.events.index')}}"><i class="fa fa-calendar"></i> All Events</a></li>
                <li class="active"><i class="fa fa-pencil"></i> Create New Event</li>
            </ol>
        </section>
        <div class="content">
            <div class="row">
                {!! Form::model($event, [
                  'method' => 'POST',
                  'route' => 'admin.events.store',
                  'id' => 'event-form'
                  ]) !!}
                @include('snippets.backend.events.form')
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@section('script')
  <script type="text/javascript">
      $(".datepicker").datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        showClear: true
      });
  </script>
@endsection
