@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Home Page
          <small><a href="{{url('/')}}">View Home Page</a></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Edit Home Page</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-sm-12">
            @include('snippets.backend.message')
          </div>
          {!! Form::model($home, [
            'method' => 'PUT',
            'route' => ['admin.home.update', $home->id],
            'id' => 'post-form',
            'files' => true
            ]) !!}
          @include('snippets.backend.home.form')

        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.home')
@endsection
