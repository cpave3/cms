@extends('layouts.backend')

@section('title', 'Blog Posts')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Posts
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-pencil"></i> Blog Posts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                <div class="pull-left">
                  <a href="{{route('admin.blog.create')}}" class="btn btn-success">Add New Post &nbsp;<i class="fa fa-plus"></i></a>
                </div>
                <div class="pull-right" style="padding: 7px 0;">
                  @include('snippets.backend.blog.links')
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body ">
                @include('snippets.backend.message')
                @include('snippets.backend.blog.message')
                @if (!$posts->count())
                  <div class="alert alert-warning">
                    <strong>No Records Found</strong>
                  </div>
                @else
                    @if ($onlyTrashed)
                      @include('snippets.backend.blog.index-table-trash')
                    @else
                      @include('snippets.backend.blog.index-table')
                    @endif
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')

@endsection
