<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail;
use App\Subscription;
use Mail as Email;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mails = Mail::all();

        return view('admin.mail.index', compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Mail $mail)
    {
        //
        $lists = Subscription::all();
        return view('admin.mail.create', compact('mail', 'lists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $lists = $data['lists'];
        // print_r($data);
        $mail = Mail::create($data);
        $mail->subscriptions()->sync($lists);

        return redirect(route('admin.mail.index'))->with('message', 'Email successfully created');
    }

    public function send($id) {
      $mail = Mail::findOrFail($id);
      $lists = $mail->subscriptions;
      $contacts = [];
      foreach ($lists as $list) {
        $subscribers = $list->subscribers;
        foreach ($subscribers as $subscriber) {
        $contacts[$subscriber->email] = $subscriber->name;
        }
      }

      print_r($contacts);

      foreach ($contacts as $email => $name) {
        Email::send(['email.test', 'email.test'], ['body' => $mail->body, 'name'=>$name], function ($m) use ($email, $name, $mail) {
              $m->from($mail->from_email, $mail->from_name);
              $m->to($email, $name)->subject($mail->subject);
          });
      }

      $mail->sent_at = date('Y-m-d H:i:s');
      $mail->save();
      return redirect(route('admin.mail.index'))->with('message', 'Email successfully sent');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $lists = Subscription::all();
        $mail = Mail::findOrFail($id);
        return view('admin.mail.edit', compact('mail', 'lists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();
        $lists = $data['lists'];
        // print_r($data);
        $mail = Mail::findOrFail($id);
        $mail->update($data);
        $mail->subscriptions()->sync($lists);

        return redirect(route('admin.mail.index'))->with('message', 'Email successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
