<?php

namespace App;

use App\Http\Requests\EventRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Event
 *
 * @mixin \Eloquent
 */
class Event extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		  'title', 'slug', 'event_date', 'expiry_date', 'location', 'excerpt', 'body'
	];

	private $numItemsPerPage = 6;

	public static function createEvent(EventRequest $request)
	{
		$slug = strtolower(str_replace(' ', '-', $request->title));
		$request->merge(['slug' => $slug, 'expiry_date' => $request->event_date]);

		Event::create($request->all());
	}

	public static function updateEvent(EventRequest $request, Event $event)
	{
		$slug = strtolower(str_replace(' ', '-', $request->title));
		$request->merge(['slug' => $slug, 'expiry_date' => $request->event_date]);
		$input = $request->all();
		$event = Event::findOrFail($event->id);
		$updateNow = $event->update($input);
	}

	public static function getFirstThreeUpcomingEvents(){
		return $event = Event::whereDate('event_date', '>=', Carbon::today()->toDateString())->orderBy('event_date', 'asc')->take(3)->get();
		//dd($event);
	}

	public static function getCurrentEvents(){
		return $event = Event::whereDate('event_date', '>=', Carbon::today()->toDateString())->orderBy('event_date', 'asc')->paginate(6);
	}

	public static function getPastEvents(){
		return $event = Event::whereDate('event_date', '<', Carbon::today()->toDateString())->orderBy('event_date', 'des')->paginate(6);
	}

	public static function getEventsAMonthAgo(){
		return $event = Event::whereDate('event_date', '<', Carbon::today()->subMonth(1)->toDateString())->orderBy('event_date', 'des')->paginate(6);
	}

	public static function  checkForUpcomingEvents($events){
		//check if there are no upcoming events
		if ($events == '[]' ){
			return true;
		} else {
			return false;
		}
	}

	public static function formatDate($eventDate){
		return Carbon::parse($eventDate)->format('l, jS F Y');
	}

	public static function toFormattedDate($eventDate){
		return Carbon::parse($eventDate)->toFormattedDateString();
	}
}
