@php
  $links = [];
@endphp
@if (isset($statusList))
  @foreach ($statusList as $key => $value)
    @if ($value)
      @php
        $selected = Request::get('status') == $key ? 'selected-status' : '';
        $links[] = "<a class=\"{$selected}\" href=\"?status=$key\"> ".ucwords($key)." ({$value})</a>";
      @endphp
    @endif
  @endforeach
@endif
{!! implode(' | ', $links) !!}
