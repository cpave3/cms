<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Page;
use App\Event;

class SearchController extends Controller
{

	private function pageQuery($query)
	{
		$pages = Page::where('title', 'LIKE', "%$query%")
			  ->orWhere('body', 'LIKE', "%$query%")
			  ->orWhere('excerpt', 'LIKE', "%$query%")
			  ->whereNotNull('published_at')
			  ->orderBy('title', 'asc')
			  ->get();

		if (count($pages) > 0) {
			foreach ($pages as $p) {
				$p["Type"] = "Pages";
			}
		}

		return $pages;
	}

	private function postQuery($query)
	{
		$posts = Post::where('title', 'LIKE', "%$query%")
			  ->orWhere('body', 'LIKE', "%$query%")
			  ->orWhere('excerpt', 'LIKE', "%$query%")
			  ->whereNotNull('published_at')
			  ->orderBy('updated_at', 'desc')
			  ->get();

		if (count($posts) > 0) {
			foreach ($posts as $p) {
				$p["Type"] = "Posts";
			}
		}

		return $posts;
	}

	private function eventsQuery($query)
	{
		$events = Event::where('title', 'LIKE', "%$query%")
			  ->orWhere('body', 'LIKE', "%$query%")
			  ->orWhere('excerpt', 'LIKE', "%$query%")
			  ->orWhere('location', 'LIKE', "%$query%")
			  ->orderBy('event_date', 'desc')
			  ->get();

		if (count($events) > 0) {
			foreach ($events as $e) {
				$e["Type"] = "Events";
			}
		}

		return $events;
	}

	public function search(Request $request)
	{

		$query = $request->get('query');
		$pagesChecked = $request->get('type-pages');
		$postsChecked = $request->get('type-posts');
		$eventsChecked = $request->get('type-events');

//		//Num items per page
//		$perPage = 5;
//
//		//Get current page from url e.g ?Page=6
//		$currentPage = LengthAwarePaginator::resolveCurrentPath();

		if ($pagesChecked && $postsChecked == false && $eventsChecked == false) {
			$results = $this->pageQuery($query);
		}
		elseif ($postsChecked && $pagesChecked == false && $eventsChecked == false) {
			$results = $this->postQuery($query);
		}
		elseif ($eventsChecked && $postsChecked == false && $pagesChecked == false) {
			$results = $this->eventsQuery($query);
		}
		elseif($pagesChecked && $postsChecked && $eventsChecked == false){
			$pages = $this->pageQuery($query);
			$posts = $this->postQuery($query);
			$results = $posts->merge($pages);
		}
		elseif($pagesChecked && $postsChecked == false && $eventsChecked){
			$pages = $this->pageQuery($query);
			$events = $this->eventsQuery($query);
			$results = $pages->merge($events);
		}
		elseif($pagesChecked == false && $postsChecked && $eventsChecked){
			$posts = $this->postQuery($query);
			$events = $this->eventsQuery($query);
			$results = $posts->merge($events);
		}
		else {
			$pages = $this->pageQuery($query);
			$posts = $this->postQuery($query);
			$events = $this->eventsQuery($query);

			//merge results into one collection
			$results = $posts->merge($events)->merge($pages);

//			//Slice the collection to get the items to display in current page
//			$currentPageSearchResults = $results->slice(($currentPage - 1) * $perPage, $perPage)->all();
//
//			//create Paginator and pass it to the view
//			$paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($results), $perPage);
		}

		return view('search.index', compact('query', 'results', 'pagesChecked', 'postsChecked', 'eventsChecked'));

	}
}