<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Upload extends Model
{
    //
    protected $fillable = [
      'name',
      'originalFile',
      'fileName',
      'captopn',
      'type',
      'alt',
      'user_id',
      'size',
      'hidden'
    ];

    public function user() {
      return $this->belongsTo('App\User');
    }

    public function pages() {
      return $this->belongsToMany('App\Page', 'page_upload');
    }

    public function posts() {
      return $this->belongsToMany('App\Post', 'post_upload');
    }

    public function getTypeNameAttribute() {
      switch ($this->type) {
        case 0:
          //images
          return 'Unassigned';
          break;
        case 1:
          //images
          return 'Image';
          break;
        case 2:
          //attachments
          return 'Attachment';
          break;
      }
    }

    public function getPathAttribute() {
      return url('/uploads')."/".$this->fileName;
    }

    public function getSizeAttribute($value) {

      if ($value < 1000000) {
        return round($value/1024, 2) . " KB";
      } elseif ($value > 1000000 && $value < 1000000000) {
        return round($value/1024/1024, 2) . " MB";
      } else {
        return round($value/1024/1024/1024, 2) . " GB";
      }

    }
}
