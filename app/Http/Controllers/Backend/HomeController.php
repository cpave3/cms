<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Home;

class HomeController extends Controller
{


    public function edit()
    {
      $home = Home::first();
      if (!isset($home)) {
        $home = Home::create();
      }
      return view('admin.home.edit', compact('home'));
    }


    public function update(Request $request, $id)
    {
        $home = Home::findOrFail($id);
        $data = $request->all();

        if ($request->hasFile('hero')) {
          // unlink(public_path('uploads/'.$home['hero']));

          $hero = $request->file('hero');
          $data['hero'] = time() . "-" . $hero->getClientOriginalName();
          $destination = 'uploads';
          $successUploaded = $hero->move($destination, $data['hero']);
        }

        $home->update($data);
        return redirect(route('admin.home.edit'))->with('message', 'Home page successfully updated');
    }


}
