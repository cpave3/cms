<div class="col-sm-8 col-md-9">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body ">
        <div class="form-group {{$errors->has('title') ? 'has-error' : ''}}">
          {!! Form::label('title')!!}
          {!! Form::text('title', null, ['class' => 'form-control'])!!}
          @if ($errors->has('title'))
            <span class="help-block">{{$errors->first('title')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('slug') ? 'has-error' : ''}}">
          {!! Form::label('slug')!!}
          <div class="input-group">
            <span class="input-group-addon">{{route('blog.index')}}/category</span>
            {!! Form::text('slug', null, ['class' => 'form-control'])!!}
          </div>
          @if ($errors->has('slug'))
            <span class="help-block">{{$errors->first('slug')}}</span>
          @endif
        </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      {!! Form::submit(($category->exists ? 'Update' : 'Save'), ['class' => 'btn btn-primary']) !!}
      <a href="{{route('admin.categories.index')}}" class="btn btn-default">Cancel</a>
    </div>
  </div>
  <!-- /.box -->
</div>
