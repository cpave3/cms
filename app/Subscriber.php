<?php

namespace App;

use App\Http\Requests\SubscribeRequest;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
	protected $fillable = [
		  'name', 'email', 'key'
	];

	public static function addSubscriber(SubscribeRequest $request)
	{
		$input = $request->all();
		$input['key'] = hash('sha256', ($input['email'])); //make the invite key as an MD5 of the email

		Subscriber::create($input);
		//dd($input['key']);
	}

	public function subscriptions() {
		return $this->belongsToMany('App\Subscription', 'subscriber_subscriptions');
	}
}
