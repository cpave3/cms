@extends('layouts.backend')

@section('title', 'Mailing Lists')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mailing Lists
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-list"></i> Mailing Lists</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                <div class="pull-left">
                  <a href="{{route('admin.lists.create')}}" class="btn btn-success">Add New List &nbsp;<i class="fa fa-plus"></i></a>
                </div>
                <div class="pull-right" style="padding: 7px 0;">
                  @include('snippets.backend.lists.links')
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body ">
                @include('snippets.backend.message')
                {{-- @include('snippets.backend.lists.message') --}}
                @if (!$subscriptions->count())
                  <div class="alert alert-warning">
                    <strong>No Records Found</strong>
                  </div>
                @else
                    @if ($onlyTrashed)
                      @include('snippets.backend.lists.index-table-trash')
                    @else
                      @include('snippets.backend.lists.index-table')
                    @endif
                @endif
              </div>
              <!-- /.box-body -->
              <div class="box-footer clearfix">
                <div class="pull-left">
                  {{-- {{$subscriptions->appends(Request::query())->render()}} --}}
                </div>
                <div class="pull-right">
                  <small>{{$subscriptionCount}} {{str_plural('List', $subscriptionCount)}}</small>
                </div>
              </div>
            </div>
            <!-- /.box -->
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $('ul.pagination').addClass('no-margin pagination-sm');
  </script>
@endsection
