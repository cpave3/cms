<div class="col-sm-8 col-md-9">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body ">
        <div class="form-group {{$errors->has('title') ? 'has-error' : ''}}">
          {!! Form::label('title')!!}
          {!! Form::text('title', null, ['class' => 'form-control'])!!}
          @if ($errors->has('title'))
            <span class="help-block">{{$errors->first('title')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('slug') ? 'has-error' : ''}}">
          {!! Form::label('slug')!!}
          <div class="input-group">
            <span class="input-group-addon">{{url('/')}}/</span>
            {!! Form::text('slug', null, ['class' => 'form-control'])!!}
          </div>
          @if ($errors->has('slug'))
            <span class="help-block">{{$errors->first('slug')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('excerpt') ? 'has-error' : ''}}">
          {!! Form::label('excerpt')!!}
          {!! Form::textarea('excerpt', null, ['class' => 'form-control excerpt', 'rows' => '3'])!!}
          @if ($errors->has('excerpt'))
            <span class="help-block">{{$errors->first('excerpt')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('body') ? 'has-error' : ''}}">
          {!! Form::label('body')!!}
          {!! Form::textarea('body', null, ['class' => 'form-control tinymce'])!!}
          @if ($errors->has('body'))
            <span class="help-block">{{$errors->first('body')}}</span>
          @endif
        </div>


    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<div class="col-sm-4 col-md-3">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">
        Publish
      </h3>
    </div>
    <div class="box-body clearfix">
      <div class="pull-left">
        <a href="#" id="draft-btn" class="btn btn-default">Save Draft</a>
      </div>
      <div class="pull-right">
        {!! Form::hidden('published_at', null, ['id' => 'published_at']) !!}
        <a href="#" id="submit-btn" class="btn btn-primary">Publish Page</a>
      </div>
    </div>
  </div>




  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Parent</h3>
    </div>
    <div class="box-body">
      <div class="form-group {{$errors->has('parent') ? 'has-error' : ''}}">
        {!! Form::select('parent', $allPages ,$page->parent_id, ['class' => 'form-control', 'placeholder' => 'Select a Parent Page'])!!}
        @if ($errors->has('parent'))
          <span class="help-block">{{$errors->first('parent')}}</span>
        @endif
      </div>
    </div>
  </div>

  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Sidebar Navigation</h3>
    </div>
    <div class="box-body">
      <div class="form-group {{$errors->has('menu_id') ? 'has-error' : ''}}">
        {!! Form::select('menu_id', App\Menu::pluck('name', 'id') ,$page->menu_id, ['class' => 'form-control', 'placeholder' => 'Select a Menu for the sidebar'])!!}
        @if ($errors->has('menu_id'))
          <span class="help-block">{{$errors->first('menu_id')}}</span>
        @endif
      </div>
    </div>
  </div>
  {!! Form::close() !!}

  @include('snippets.backend.widgets.upload')



</div>
