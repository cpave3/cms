<div class="col-sm-12">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body ">
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
          {!! Form::label('name', 'List Name')!!}
          {!! Form::text('name', null, ['class' => 'form-control', 'required'])!!}
          @if ($errors->has('name'))
            <span class="help-block">{{$errors->first('name')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('from_email') ? 'has-error' : ''}}">
          {!! Form::label('from_email', 'Default From Email Address')!!}
          {!! Form::text('from_email', null, ['class' => 'form-control', 'required'])!!}
          @if ($errors->has('from_email'))
            <span class="help-block">{{$errors->first('from_email')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('from_name') ? 'has-error' : ''}}">
          {!! Form::label('from_name', 'Default From Name')!!}
          {!! Form::text('from_name', null, ['class' => 'form-control', 'required'])!!}
          @if ($errors->has('from_name'))
            <span class="help-block">{{$errors->first('from_name')}}</span>
          @endif
        </div>

        <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
          {!! Form::label('description', 'Remind people how they signed up to your list')!!}
          {!! Form::textarea('description', null, ['class' => 'form-control', 'required', 'rows'=>'3'])!!}
          @if ($errors->has('description'))
            <span class="help-block">{{$errors->first('description')}}</span>
          @endif
        </div>




    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
      <div class="pull-right">
        {!! Form::submit(($subscription->exists ? 'Update' : 'Save'), ['class' => 'btn btn-primary']) !!}
        <a href="{{route('admin.lists.index')}}" class="btn btn-default">Cancel</a>
      </div>
    </div>
  </div>
  <!-- /.box -->
</div>
