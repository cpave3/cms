@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Mailing List
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.lists.index')}}"><i class="fa fa-list"></i> Mailing Lists</a></li>
        <li class="active"><i class="fa fa-pencil"></i> &nbsp;Edit List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {!! Form::model($subscription, [
            'method' => 'PUT',
            'route' => ['admin.lists.update', $subscription->id]
            ]) !!}
          @include('snippets.backend.lists.form')
          {!! Form::close() !!}
        </div>
        <div class="row">
          {{-- Table of current subscriptions to this list --}}
          @include('snippets.backend.lists.table')
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  {{-- @include('snippets.backend.scripts.blog') --}}
@endsection
