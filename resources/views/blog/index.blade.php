@extends('layouts.pages')
@section('content')
  <br>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
              @if(! $posts->count())
                <div class="alter alert-info">
                  <p>There is nothing to Display</p>
                </div>
              @else
                @if(isset($categoryName))
                  <div class="alert alert-info">
                    <p>Category: <strong>{{$categoryName}}</strong></p>
                  </div>
                @endif
                @foreach($posts as $post)
                  <article class="post-item">
                    @if($post->image)
                      <div class="post-item-image">
                          <a href="{{route('blog.single', $post->slug)}}">
                              <img src="{{$post->image_url}}" alt="">
                          </a>
                      </div>
                    @endif
                      <div class="post-item-body">
                          <div class="padding-10">
                              <h2><a href="{{route('blog.single', $post->slug)}}">{{$post->title}}</a></h2>
                              <p>{!! $post->excerptHtml !!}</p>
                          </div>

                          <div class="post-meta padding-10 clearfix">
                              <div class="pull-left">
                                  <ul class="post-meta-group">
                                      <li><i class="fa fa-user"></i><a href="{{route('blog.author', $post->author->slug)}}"> {{$post->author->name}}</a></li>
                                      <li><i class="fa fa-clock-o"></i><time> {{$post->date}}</time></li>
                                      <li><i class="fa fa-folder"></i><a href="{{route('blog.category', $post->category->slug)}}"> {{$post->category->title}}</a></li>
                                      {{-- <li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li> --}}
                                  </ul>
                              </div>
                              <div class="pull-right">
                                  <a href="{{route('blog.single', $post->slug)}}">Continue Reading &raquo;</a>
                              </div>
                          </div>
                      </div>
                  </article>
                @endforeach
              @endif
                {{$posts->links()}}
                {{-- <nav>
                  <ul class="pager">
                    <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Newer</a></li>
                    <li class="next"><a href="#">Older <span aria-hidden="true">&rarr;</span></a></li>
                  </ul>
                </nav> --}}
            </div>
            @include('snippets.sidebar-blog-right')
        </div>
    </div>
    @endsection
