@extends('layouts.backend')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Change {{ $user->name }}'s Password
             </h1>
            <ol class="breadcrumb">
                <li><i class="fa fa-user"></i> Users</li>
                <li class="active"><i class="fa fa-pencil"></i> Change Password</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body ">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                              {!! Form::open([
                                                'method' => 'PATCH',
                                                'action' => ['Backend\AdminUserController@updatePassword', $user->id]
                                                ]) !!}

                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label for="password" class="col-md-4 control-label">New Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control" name="password">

                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <a class="btn bg-gray-light" href="{{ url('admin/users') }}">Cancel</a>
                                                            <button type="submit" class="btn btn-primary">
                                                                Update Password
                                                            </button>
                                                        </div>
                                                    </div>
                                                {{Form::close()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
