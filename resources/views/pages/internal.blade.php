@extends('layouts.pages')
@section('title', 'Queensland Advocacy Incorporated')
@section('content')
  <section id="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1>{{$page->title}}</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9">

          {!!$page->body!!}
        </div>

        <div class="col-sm-3">

          @if (isset($page->menu))
            <div>
              <style media="screen" scoped>
                ul {
                  list-style: none;
                  padding-left: 10px;
                }

                a {
                  padding: 5px;
                  /*display: block;*/
                }
              </style>
              <h3>Navigation</h3>
              @php eval('$m = '.$page->menu->data.';'); @endphp

              @if (!empty($m))
                <ul>
                  @foreach ($m as $root)

                    @if ($root[0] == "x1" || $root[0] == "x2" || $root[0] == "x3")
                      @php
                        switch ($root[0]) {
                          case 'x1':
                            echo "<li><a href=\"".url('/')."\">Home</a>
                                  <ul>";
                            break;
                          case 'x2':
                          echo "<li><a href=\"".url('/blog')."\">Blog</a>
                                <ul>";
                            break;
                          case 'x3':
                          echo "<li><a href=\"".url('/events')."\">Events</a>
                                <ul>";
                            break;
                        }
                      @endphp
                    @else
                      @php $p = App\Page::find($root[0]); @endphp
                      <li><a href="{{url('/').'/'.$p->path}}">{{$root[1]}}</a>
                        <ul>
                    @endif

                        @foreach ($root[2] as $child1)

                          @if ($child1[0] == "x1" || $child1[0] == "x2" || $child1[0] == "x3")
                            @php
                              switch ($child1[0]) {
                                case 'x1':
                                  echo "<li><a href=\"".url('/')."\">Home</a>
                                        <ul>";
                                  break;
                                case 'x2':
                                echo "<li><a href=\"".url('/blog')."\">Blog</a>
                                      <ul>";
                                  break;
                                case 'x3':
                                echo "<li><a href=\"".url('/events')."\">Events</a>
                                      <ul>";
                                  break;
                              }
                            @endphp
                          @else
                            @php $p = App\Page::find($child1[0]); @endphp
                            <li><a href="{{url('/').'/'.$p->path}}">{{$child1[1]}}</a>
                              <ul>
                          @endif

                              @foreach ($child1[2] as $child2)
                                @if ($child2[0] == "x1" || $child2[0] == "x2" || $child2[0] == "x3")
                                  @php
                                    switch ($child2[0]) {
                                      case 'x1':
                                        echo "<li><a href=\"".url('/')."\">Home</a>";
                                        break;
                                      case 'x2':
                                      echo "<li><a href=\"".url('/blog')."\">Blog</a>";
                                        break;
                                      case 'x3':
                                      echo "<li><a href=\"".url('/events')."\">Events</a>";
                                        break;
                                    }
                                  @endphp
                                @else
                                  @php $p = App\Page::find($child2[0]); @endphp
                                  <li><a href="{{url('/').'/'.$p->path}}">{{$child2[1]}}</a>
                                @endif
                              @endforeach
                            </ul>
                          </li>
                        @endforeach
                      </ul>
                    </li>
                  @endforeach
                </ul>
              @endif

            </div>
          @endif

          @if ($page->attachments->count() > 0)
            <style scoped>
            /* TODO:  SASSIFY */
              h3 {margin: 0;}
            </style>



            <h3>Attachments</h3>
            <ul>
              @foreach ($page->attachments as $attachment)
                @if ($attachment->hidden == 0)
                  <li><a href="{{$attachment->path}}" target="_blank"><abbr title="{{$attachment->name}}">{{strlen($attachment->name) <= 35 ? $attachment->name : substr($attachment->name,0,35) . "..."}}</abbr></a>
                @endif
              @endforeach
            </ul>
          @endif
        </div>

      </div>
    </div>
  </section>
@endsection
