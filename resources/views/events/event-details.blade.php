@extends('layouts.pages')
@section('title', $event->title)
@section('content')

    <button class="past-events-link">
        <a href="{{url('/events')}}">
            BACK TO EVENTS
        </a>
    </button>

    <section class="event-banner" data-speed="6" style="background: none;"></section>

    <div class="col-md-10 col-md-offset-2">
        <div class="event-details-container">
            <div class="row"> <!-- START events -->
                <div class="event-image col-md-12 col-lg-8">
                    <img src="../img/header-bg.jpg" alt="event image">
                </div>

                <div class="col-md-12 col-lg-4">
                    <section class="event-information">
                        <h1>{{ $event->title }}</h1>
                        <p><strong><i class="fa fa-calendar" aria-hidden="true"></i> When: {{ $event::toFormattedDate($event->event_date) }}</strong></p>
                        <p><strong><i class="fa fa-map-marker" aria-hidden="true"></i> Location: {{ $event->location }}</strong></p>
                    </section>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-10 col-md-offset-1">
        <div class="event-details-content-container container">
            <h2 class="event-excerpt">{{ $event->excerpt }}</h2>
            <section class="event-details">
                <article>
                    {!! $event->body !!}
                    {{-- <iframe src="https://www.youtube.com/embed/Bg9GXtYPG5E" frameborder="0" allowfullscreen></iframe> --}}
                </article>
            </section>
        </div>
    </div>
@endsection
