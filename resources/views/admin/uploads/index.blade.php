@extends('layouts.backend')

@section('title', 'Uploads')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Uploads
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><i class="fa fa-upload"></i> Uploads</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header clearfix">
                <div class="pull-left">
                  <a href="{{route('admin.blog.create')}}" class="btn btn-success">Add New Upload &nbsp;<i class="fa fa-plus"></i></a>
                </div>
                <div class="pull-right" style="padding: 7px 0;">
                  @include('snippets.backend.uploads.links')
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body ">
                @include('snippets.backend.message')
                @include('snippets.backend.uploads.message')
                @if (!$uploads->count())
                  <div class="alert alert-warning">
                    <strong>No Records Found</strong>
                  </div>
                @else
                  <?php // TODO: Replace table with a Grid view ?>
                  @include('snippets.backend.uploads.index-table')
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')

@endsection
