<table class="table table-bordered">
  <thead>
    <tr>
      <td width="80">Action</td>
      <td width="200">Name</td>
      <td width="120">Items</td>
    </tr>
  </thead>
  <tbody>
    @foreach ($menus as $menu)
      <tr>
        <td>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.menus.destroy', $menu->id]]) !!}
        <a href="{{route('admin.menus.edit', $menu->id)}}" class="btn btn-xs btn-default">
          <i class="fa fa-edit"></i>
        </a>
        <button type="submit" class="btn btn-xs btn-danger">
          <i class="fa fa-trash"></i>
        </button>
        {!! Form::close() !!}
        </td>
        <td>{{$menu->name}}</td>
        <td>{{$menu->data}}</td>
      </tr>
    @endforeach
  </tbody>
</table>
