<div class="col-sm-8 col-md-9">
  <div class="box">
    <!-- /.box-header -->
    <div class="box-body ">
      <div class="form-group {{$errors->has('hero') ? 'has-error' : ''}}">
        {!! Form::label('hero')!!}
        <input type="file" name="hero">
        @if ($errors->has('hero'))
          <span class="help-block">{{$errors->first('hero')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('hero_text') ? 'has-error' : ''}}">
        {!! Form::label('hero_text')!!}
        {!! Form::textarea('hero_text', null, ['class' => 'form-control tinymce', 'rows' => '3'])!!}
        @if ($errors->has('hero_text'))
          <span class="help-block">{{$errors->first('hero_text')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('red1_text') ? 'has-error' : ''}}">
        {!! Form::label('red1_text')!!}
        {!! Form::textarea('red1_text', null, ['class' => 'form-control', 'rows' => '3'])!!}
        @if ($errors->has('red1_text'))
          <span class="help-block">{{$errors->first('red1_text')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('red1_link') ? 'has-error' : ''}}">
        {!! Form::label('red1_link')!!}
        {!! Form::text('red1_link', null, ['class' => 'form-control'])!!}
        @if ($errors->has('red1_link'))
          <span class="help-block">{{$errors->first('red1_link')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square1_text') ? 'has-error' : ''}}">
        {!! Form::label('square1_text')!!}
        {!! Form::textarea('square1_text', null, ['class' => 'form-control tinymce', 'rows' => '3'])!!}
        @if ($errors->has('square1_text'))
          <span class="help-block">{{$errors->first('square1_text')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square1_link') ? 'has-error' : ''}}">
        {!! Form::label('square1_link')!!}
        {!! Form::text('square1_link', null, ['class' => 'form-control'])!!}
        @if ($errors->has('square1_link'))
          <span class="help-block">{{$errors->first('square1_link')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square2_text') ? 'has-error' : ''}}">
        {!! Form::label('square2_text')!!}
        {!! Form::textarea('square2_text', null, ['class' => 'form-control tinymce', 'rows' => '3'])!!}
        @if ($errors->has('square2_text'))
          <span class="help-block">{{$errors->first('square2_text')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square2_link') ? 'has-error' : ''}}">
        {!! Form::label('square2_link')!!}
        {!! Form::text('square2_link', null, ['class' => 'form-control'])!!}
        @if ($errors->has('square2_link'))
          <span class="help-block">{{$errors->first('square2_link')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square3_text') ? 'has-error' : ''}}">
        {!! Form::label('square3_text')!!}
        {!! Form::textarea('square3_text', null, ['class' => 'form-control tinymce', 'rows' => '3'])!!}
        @if ($errors->has('square3_text'))
          <span class="help-block">{{$errors->first('square3_text')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('square3_link') ? 'has-error' : ''}}">
        {!! Form::label('square3_link')!!}
        {!! Form::text('square3_link', null, ['class' => 'form-control'])!!}
        @if ($errors->has('square3_link'))
          <span class="help-block">{{$errors->first('square3_link')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('footer_contact') ? 'has-error' : ''}}">
        {!! Form::label('footer_contact')!!}
        {!! Form::textarea('footer_contact', null, ['class' => 'form-control', 'rows' => '3'])!!}
        @if ($errors->has('footer_contact'))
          <span class="help-block">{{$errors->first('footer_contact')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('top_menu_id') ? 'has-error' : ''}}">
        {!! Form::select('top_menu_id', App\Menu::pluck('name', 'id') ,$home->top_menu_id, ['class' => 'form-control', 'placeholder' => 'Select a Menu for the Top Nav'])!!}
        @if ($errors->has('top_menu_id'))
          <span class="help-block">{{$errors->first('top_menu_id')}}</span>
        @endif
      </div>

      <div class="form-group {{$errors->has('footer_menu_id') ? 'has-error' : ''}}">
        {!! Form::select('footer_menu_id', App\Menu::pluck('name', 'id') ,$home->footer_menu_id, ['class' => 'form-control', 'placeholder' => 'Select a Menu for the Footer'])!!}
        @if ($errors->has('footer_menu_id'))
          <span class="help-block">{{$errors->first('footer_menu_id')}}</span>
        @endif
      </div>

      <div class="form-group">
        <input type="submit" value="Submit" class="btn btn-success">
      </div>

      {!! Form::close() !!}

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
