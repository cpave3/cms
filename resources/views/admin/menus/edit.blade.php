@extends('layouts.backend')

@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('admin.menus.index')}}"><i class="fa fa-list"></i> Menu</a></li>
        <li class="active"><i class="fa fa-file-text"></i> &nbsp;Edit Menu</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          {{-- {!! Form::model($menu, [
            'method' => 'PUT',
            'route' => ['admin.menus.update', $menu->id],
            'files' => TRUE,
            'id' => 'post-form'
            ]) !!}
          @include('snippets.backend.menus.form')
          {!! Form::close() !!} --}}
          <div class="col-sm-3">
            <div class="box">
              <div class="box-body">
                <h4>Insert new menu items</h4>



                <style media="screen" scoped>
                  .pages-list {
                    max-height: 250px;
                    overflow-y: scroll;
                    border: 1px solid #e3e3e3;
                  }
                </style>

                <form id="search-form" method="post" action="{{route('admin.menus.data')}}">
                  {{csrf_field()}}
                  <input id="keyword" type="text" class="form-control" name="word" placeholder="Filter search results">
                </form>

                @if ($pages)
                  <div class="ajax-box">
                    <ul class="pages-list">
                      <li data-id="x1"><a href="#">Home Page</a></li>
                      <li data-id="x2"><a href="#">Blog</a></li>
                      <li data-id="x3"><a href="#">Events</a></li>
                      @foreach ($pages as $page)
                        <li data-id="{{$page->id}}"><a href="#">{{$page->title}}</a></li>
                      @endforeach
                    </ul>
                  </div>
                @endif

                <hr>
                <form id="data-in" class="clearfix">
                  <div class="form-group">
                    <label for="display_text">
                      Display Text
                    </label>
                    <input type="text" name="display_text" id="display_text" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="page_id">
                      Page ID
                    </label>
                    <input type="text" name="page_id" id="page_id" class="form-control" readonly="readonly">
                  </div>
                  <div class="form-group pull-right">
                    <input type="submit" value="Insert" class="btn btn-primary">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="box">
              <div class="box-body">
              <style type="text/css" scoped>

a,a:visited {
 color: #4183C4;
 text-decoration: none;
}

a:hover {
 text-decoration: underline;
}

pre,code {
 font-size: 12px;
}

pre {
 width: 100%;
 overflow: auto;
}

small {
 font-size: 90%;
}

small code {
 font-size: 11px;
}

.placeholder {
 outline: 1px dashed #4183C4;
}

.mjs-nestedSortable-error {
 background: #fbe3e4;
 border-color: transparent;
}

#tree {
 width: 550px;
 margin: 0;
}

ol {
 max-width: 450px;
 padding-left: 25px;
}

ol.sortable,ol.sortable ol {
 list-style-type: none;
}

.sortable li div {
 border: 1px solid #d4d4d4;
 -webkit-border-radius: 3px;
 -moz-border-radius: 3px;
 border-radius: 3px;
 cursor: move;
 border-color: #D4D4D4 #D4D4D4 #BCBCBC;
 margin: 0;
 padding: 3px;
}

li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
 border-color: #999;
}

.disclose, .expandEditor {
 cursor: pointer;
 width: 20px;
 display: none;
}

.sortable li.mjs-nestedSortable-collapsed > ol {
 display: none;
}

.sortable li.mjs-nestedSortable-branch > div > .disclose {
 display: inline-block;
}

.sortable span.ui-icon {
 display: inline-block;
 margin: 0;
 padding: 0;
}

.menuDiv {
 background: #EBEBEB;
}

.menuEdit {
 background: #FFF;
}

.itemTitle {
 vertical-align: middle;
 cursor: pointer;
}

.deleteMenu {
 float: right;
 cursor: pointer;
}

h1 {
 font-size: 2em;
 margin-bottom: 0;
}

h2 {
 font-size: 1.2em;
 font-weight: 400;
 font-style: italic;
 margin-top: .2em;
 margin-bottom: 1.5em;
}

h3 {
 font-size: 1em;
 margin: 1em 0 .3em;
}

p,ol,ul,pre,form {
 margin-top: 0;
 margin-bottom: 1em;
}

dl {
 margin: 0;
}

dd {
 margin: 0;
 padding: 0 0 0 1.5em;
}

code {
 background: #e5e5e5;
}

input {
 vertical-align: text-bottom;
}

.notice {
 color: #c33;
}
</style>

<section id="demo">
  <h4>Build your menu here</h4>
  <hr>
  <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded" id="menu-builder">
    @if (isset($menu->data))
      @php
        eval('$m = '.$menu->data.';');
        $i = 0;
      @endphp
      @foreach ($m as $root)
          <li class="mjs-nestedSortable-leaf" id="menuItem_{{$i}}" data-id="{{$root[0]}}">
           <div class="menuDiv">
             <span>
               <span data-id="{{$root[0]}}" class="itemTitle">{{$root[1]}}</span>
               <span title="Click to delete item." data-id="{{$i++}}" class="deleteMenu ui-icon ui-icon-closethick">
                 <span></span>
               </span>
             </span>
           </div>
           @if (!empty($root[2]))
             <ol>
               @foreach ($root[2] as $child1)
                   <li class="mjs-nestedSortable-leaf" id="menuItem_{{$i}}" data-id="{{$child1[0]}}">
                    <div class="menuDiv">
                      <span>
                        <span data-id="{{$child1[0]}}" class="itemTitle">{{$child1[1]}}</span>
                        <span title="Click to delete item." data-id="{{$i++}}" class="deleteMenu ui-icon ui-icon-closethick">
                          <span></span>
                        </span>
                      </span>
                    </div>

                    @if (!empty($child1[2]))
                      <ol>
                        @foreach ($child1[2] as $child2)
                            <li class="mjs-nestedSortable-leaf" id="menuItem_{{$i}}" data-id="{{$child2[0]}}">
                             <div class="menuDiv">
                               <span>
                                 <span data-id="{{$child2[0]}}" class="itemTitle">{{$child2[1]}}</span>
                                 <span title="Click to delete item." data-id="{{$i++}}" class="deleteMenu ui-icon ui-icon-closethick">
                                   <span></span>
                                 </span>
                               </span>
                             </div>
                             </li>
                        @endforeach
                      </ol>
                    @endif
                    </li>
               @endforeach
             </ol>
           @endif
           </li>
      @endforeach
    @else
      {{-- <li class="mjs-nestedSortable-leaf" id="menuItem_3">
      <div class="menuDiv">
        <span>
          <span data-id="3" class="itemTitle">Test Item</span>
          <span title="Click to delete item." data-id="3" class="deleteMenu ui-icon ui-icon-closethick">
            <span></span>
          </span>
        </span>
      </div>
      </li> --}}
    @endif
   </ol>
   <hr>


      <form action="{{route('admin.menus.update', $menu->id)}}" method="post" id="data-out">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <textarea id="data-field" name="data" rows="8" cols="80" style="display: none;"></textarea>
        {{-- <button id="json" type="button" name="button">JSON</button> --}}
        <div class="pull-right">
          <input type="submit" name="submit" value="Save" id="data-out-submit" class="btn btn-success">
        </div>
      </form>
  </section><!-- END #demo -->

              </div>
            </div>
          </div>
        </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
  @include('snippets.backend.scripts.blog')
  <script type="text/javascript">
    items = 0;

    $(document).ready(function() {
      $("#menu-builder li").each(function() {
        items++;
      });
      console.log(items);
    });


    function procList() {
      $(".pages-list>li").on('click', function() {

        value = $(this).text();
        id = $(this).data("id");

        $("#display_text").val(value);
        $("#page_id").val(id);
      });
    }

    procList();

    $("#data-in").on("submit", function(e) {
      e.preventDefault();
      display_text = $("#display_text").val();
      page_id = $("#page_id").val();

      item =  '<li class="mjs-nestedSortable-leaf" id="menuItem_'+items+'" data-id="'+page_id+'">'+
              ' <div class="menuDiv"> <span> <span data-id="'+items+'" class="itemTitle">'+display_text+'</span> '+
              '<span title="Click to delete item." data-id="'+items+'" class="deleteMenu ui-icon ui-icon-closethick"> '+
              '<span></span> </span> </span> </div></li>';

      $("#menu-builder").append(item);

      items++;

      $('.deleteMenu').click(function(){
        var id = $(this).attr('data-id');
        $('#menuItem_'+id).remove();
      });


    });
  </script>

<link rel="stylesheet" href="/plugins/jquery-ui/jquery-ui.css" />
<script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/plugins/nested-sortable/jquery.mjs.nestedSortable.js"></script>

<script>
  $().ready(function(){
    var ns = $('ol.sortable').nestedSortable({
      forcePlaceholderSize: true,
      handle: 'div',
      helper:	'clone',
      items: 'li',
      opacity: .6,
      placeholder: 'placeholder',
      revert: 250,
      tabSize: 25,
      tolerance: 'pointer',
      toleranceElement: '> div',
      maxLevels: 3,
      isTree: true,
      expandOnHover: 700,
      startCollapsed: false,
      change: function(){
        console.log('Relocated item');
      }
    });

    $('.deleteMenu').attr('title', 'Click to delete item.');

    $('.deleteMenu').click(function(){
      var id = $(this).attr('data-id');
      $('#menuItem_'+id).remove();
    });

    function compileMenu() {
      arr = [];

      $("#menu-builder>li").each(function() {
        //Level 0 start
        arr_l1 = [];
        arr_l2 = [];
        $(this).children('ol').children('li').each(function() {
          //level 1 start
          $(this).children('ol').children('li').each(function() {
            //Level 2 start
            //Level 2 Data
            item = [
              $(this).data("id"),
              $(this).find('.itemTitle').contents().get(0).nodeValue
              //3 levels only, no children past here
            ];
            arr_l2.push(item);
          });
          //Level 1 data
          item = [
            $(this).data("id"),
            $(this).find('.itemTitle').contents().get(0).nodeValue,
            arr_l2 //children
          ];
          arr_l1.push(item);
        });
        //Level 0 Data
        item = [
          $(this).data("id"),
          $(this).find('.itemTitle').contents().get(0).nodeValue,
          arr_l1
        ];
        arr.push(item);
      });
      $("#data-field").val(JSON.stringify(arr));
      // console.log(JSON.stringify(arr));
    }

    $("#data-out").on('submit', function(e) {
      // e.preventDefault;
      console.log('prevented');
      compileMenu();
      return true;
    });

  });



</script>

<script type="text/javascript">
//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#keyword');

//on keyup, start the countdown
$input.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
  clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping () {
  $("#search-form").submit();
}



$('#search-form').ajaxForm({
  success: function(res) {
    $(".ajax-box").html(res);
    procList();
  }
});
</script>
@endsection
